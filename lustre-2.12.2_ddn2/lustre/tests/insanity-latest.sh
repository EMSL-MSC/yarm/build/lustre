#!/bin/bash

set -e

LUSTRE=${LUSTRE:-$(cd $(dirname $0)/..; echo $PWD)}
. $LUSTRE/tests/test-framework.sh
init_test_env $@
. ${CONFIG:=$LUSTRE/tests/cfg/$NAME.sh}
init_logging

build_test_filter

assert_env MOUNT LFS DIR

tfile=f${testnum}.${TESTSUITE}

test_1()
{
	echo c > /proc/sysrq-trigger
}
run_test 1 "===  crash client ==="

test_2()
{
	do_facet $SINGLEMDS "echo c > /proc/sysrq-trigger"
}
run_test 2 "===  crash MDS ==="

test_3()
{
	do_facet ost1 "echo c > /proc/sysrq-trigger"
}
run_test 3 "===  crash ost1 ==="

test_4()
{
	reboot &
}
run_test 4 "===  reboot client ==="

test_5()
{
	sleep 3600
}
run_test 5 "===  keep silent for 1 hour ==="

test_6()
{
	return 0
}
run_test 6 "multiple line of \
subtest \
tittle"

echo  "$(date +'%F %H:%M:%S'): insanity-autotest test exiting"

complete $SECONDS
check_and_cleanup_lustre
exit_status

