/*
 * Copyright (c) 2019, DDN Storage Corporation.
 */
/*
 * lustre/utils/llamigo.c
 *
 * Tool to make replicas for aged files
 *
 * Author: Qian Yingjin <qian@ddn.com>
 * Author: Alexey Zhuravlev <bzzz@whamcloud.com>
 */

/*
 * TODO
 *  - lamigo.8 man page
 *  - striping info & attrs w/o local mount by special ioctl
 *  - resync frequently modified files once a day
 *  - scan by request (if changelog was missing, etc)
 *  - check the agents before main loop
 *  - V1 striping has no pool info, be able to check for
 *    specific OSTs, track pool configuration runtime
 *  - handle replication failures
 *  - detect died agents
 *  - exclude rules based on credential/name/job/etc
 *  - choose optimal agent depending on src/tgt OSTs
 *  - auto-config with lfs df -v to find flash OSTs
 *  - tests
 */
#include <stdlib.h>
#include <errno.h>
#include <getopt.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <poll.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <time.h>
#include <linux/unistd.h>
#include <linux/kernel.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <lustre/lustreapi.h>
#include <linux/lustre/lustre_idl.h>
#include <linux/lustre/lustre_fid.h>
#include <libcfs/util/hash.h>
#include <libcfs/util/list.h>
#include <libcfs/util/parser.h>

#define container_of(ptr, type, member) ({                      \
	const typeof(((type *) 0)->member) * __mptr = (ptr);     \
	(type *) ((char *) __mptr - offsetof(type, member)); })

#define CHLG_POLL_INTV	60
#define REC_MIN_AGE	600
#define DEF_CACHE_SIZE	(256 * 1048576) /* 256MB */

struct options {
	const char	*o_chlg_user;
	const char	*o_mdtname;
	const char	*o_mntpt;
	const char	*o_src_pool;
	int		 o_src_pool_len;
	char		*o_tgt_pool;
	int		 o_tgt_pool_len;
	bool		 o_daemonize;
	int		 o_verbose;
	int		 o_min_age;
	unsigned long	 o_cached_fid_hiwm; /* high watermark */
	unsigned long	 o_batch_sync_cnt;
	char		*o_dump_file;
	int		 o_chlg_clear_frequency;
};

struct options opt;

struct stats {
	unsigned long s_read;	/* llog records read */
	unsigned long s_skipped; /* llog records skipped */
	unsigned long s_processed; /* llog records processed */
	unsigned long s_removed; /* removed by unlink */
	unsigned long s_dups;
	unsigned long s_spawned;
};

struct stats stats = { 0 };

struct fid_rec {
	struct hlist_node	fr_node;
	struct list_head	fr_link;
	lustre_fid		fr_fid;
	__u64			fr_time;
	__u64			fr_index;
};

static const int fid_hash_shift = 6;

#define FID_HASH_ENTRIES	(1 << fid_hash_shift)
#define FID_ON_HASH(f)		(!hlist_unhashed(&(f)->fr_node))

#if __BITS_PER_LONG == 32
#define FID_HASH_FN(f)	(hash_long(fid_flatten32(f), fid_hash_shift))
#elif __BITS_PER_LONG == 64
#define FID_HASH_FN(f)	(hash_long(fid_flatten(f), fid_hash_shift))
#else
#error Wordsize not 32 or 64
#endif

struct list_head lamigo_job_list;
struct lamigo_head {
	struct hlist_head	*lh_hash;
	struct list_head	lh_list; /* ordered list by record index */
	unsigned long	lh_cached_count;
} head;

__u64 lamigo_last_processed_idx = 0; /* changelog index */
struct list_head lamigo_agent_list;
int lamigo_max_jobs = 0; /* max jobs for all agents */
int lamigo_jobs_running = 0; /* jobs running at the moment */
unsigned long cache_size = DEF_CACHE_SIZE;
char *ssh;
char *prog = NULL;

FILE *dfile = NULL;

#define DEBUG(fmt, args...)				\
	do {						\
		if (dfile) {				\
			fprintf(dfile, fmt, ##args);	\
			fflush(dfile);			\
		}					\
	} while (0);

DIR *dotlustre = NULL;

static void usage(void)
{
	printf("\nUsage: %s [options] -u <userid> -m <mdtdev> <mntpt>\n"
	       "options:\n"
	       "\t-d, --daemonize\n"
	       "\t-a, --min-age, min age before a record is processed.\n"
	       "\t-c, --max-cache, percentage of the memroy used for cache.\n"
	       "\t-f, --config, config file\n"
	       "\t-m, --mdt, MDT service to listen to\n"
	       "\t-u, --user, changelog id\n"
	       "\t-g, --agent, agent to migrate data\n"
	       "\t-s, --src, source OST pool\n"
	       "\t-t, --tgt, target OST pool\n"
	       "\t-w, --dump, file to dump stats to\n"
	       "\t-v, --verbose, produce more verbose ouput\n",
	       prog);
	exit(0);
}

static inline __u64 fid_flatten(const struct lu_fid *fid)
{
	__u64 ino;
	__u64 seq;

	if (fid_is_igif(fid)) {
		ino = lu_igif_ino(fid);
		return ino;
	}

	seq = fid_seq(fid);

	ino = (seq << 24) + ((seq >> 24) & 0xffffff0000ULL) + fid_oid(fid);

	return ino ?: fid_oid(fid);
}

/**
 * map fid to 32 bit value for ino on 32bit systems.
 */
static inline __u32 fid_flatten32(const struct lu_fid *fid)
{
	__u32 ino;
	__u64 seq;

	if (fid_is_igif(fid)) {
		ino = lu_igif_ino(fid);
		return ino;
	}

	seq = fid_seq(fid) - FID_SEQ_START;

	/* Map the high bits of the OID into higher bits of the inode number so
	 * that inodes generated at about the same time have a reduced chance
	 * of collisions. This will give a period of 2^12 = 1024 unique clients
	 * (from SEQ) and up to min(LUSTRE_SEQ_MAX_WIDTH, 2^20) = 128k objects
	 * (from OID), or up to 128M inodes without collisions for new files.
	 */
	ino = ((seq & 0x000fffffULL) << 12) + ((seq >> 8) & 0xfffff000) +
	      (seq >> (64 - (40-8)) & 0xffffff00) +
	      (fid_oid(fid) & 0xff000fff) + ((fid_oid(fid) & 0x00fff000) << 8);

	return ino ?: fid_oid(fid);
}

static void fid_hash_del(struct fid_rec *f)
{
	if (FID_ON_HASH(f))
		hlist_del_init(&f->fr_node);
}

static void fid_hash_add(struct fid_rec *f)
{
	assert(!FID_ON_HASH(f));
	hlist_add_head(&f->fr_node, &head.lh_hash[FID_HASH_FN(&f->fr_fid)]);
}

static struct fid_rec *fid_hash_find(const lustre_fid *fid)
{
	struct hlist_head *hash_list;
	struct hlist_node *entry, *next;
	struct fid_rec *f;

	hash_list = &head.lh_hash[FID_HASH_FN(fid)];
	hlist_for_each_entry_safe(f, entry, next, hash_list, fr_node) {
		assert(FID_ON_HASH(f));
		if (lu_fid_eq(fid, &f->fr_fid))
			return f;
	}

	return NULL;
}

void lamigo_usr1_handle(int sig)
{
	FILE *f;
	DEBUG("dump to %s\n", opt.o_dump_file);
	if (opt.o_dump_file == NULL)
		return;
	f = fopen(opt.o_dump_file, "w");
	if (!f) {
		DEBUG("can't open dump file\n");
		return;
	}
	fprintf(f,
		"read %lu\nskipped %lu\nprocessed %lu\n"
		"removed %lu\ndups %lu\nspawned %lu\n",
		stats.s_read, stats.s_skipped, stats.s_removed,
		stats.s_dups, stats.s_processed,
		stats.s_spawned);
	fflush(f);
	fclose(f);
}

void lamigo_usr2_handle(int sig)
{
}

void lamigo_chld_handle(int sig)
{
}

static int lamigo_setup(void)
{
	struct sigaction act;
	sigset_t set;
	int i;

	/* set llapi message level */
	llapi_msg_set_level(opt.o_verbose);

	memset(&head, 0, sizeof(head));
	head.lh_hash = malloc(sizeof(struct hlist_head) * FID_HASH_ENTRIES);
	if (head.lh_hash == NULL) {
		llapi_err_noerrno(LLAPI_MSG_ERROR,
				 "failed to alloc memory for hash (%zu).",
				 sizeof(struct hlist_head) * FID_HASH_ENTRIES);
		return -ENOMEM;
	}

	for (i = 0; i < FID_HASH_ENTRIES; i++)
		INIT_HLIST_HEAD(&head.lh_hash[i]);

	INIT_LIST_HEAD(&head.lh_list);
	INIT_LIST_HEAD(&lamigo_job_list);
	INIT_LIST_HEAD(&lamigo_agent_list);

	memset(&opt, 0, sizeof(opt));
	opt.o_verbose = LLAPI_MSG_INFO;
	opt.o_min_age = REC_MIN_AGE;
	opt.o_chlg_clear_frequency = 4096;

	memset(&act, 0, sizeof(act));
	act.sa_handler = lamigo_usr1_handle;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	act.sa_mask = set;
	sigaction(SIGUSR1, &act, 0);

	memset(&act, 0, sizeof(act));
	act.sa_handler = lamigo_usr2_handle;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR2);
	act.sa_mask = set;
	sigaction(SIGUSR2, &act, 0);

	memset(&act, 0, sizeof(act));
	act.sa_handler = lamigo_chld_handle;
	sigemptyset(&set);
	sigaddset(&set, SIGCHLD);
	act.sa_mask = set;
	sigaction(SIGCHLD, &act, 0);

	ssh = getenv("RSH");
	if (!ssh)
		ssh = "/usr/bin/ssh";

	return 0;
}

static void lamigo_cleanup(void)
{
	free(head.lh_hash);
}

struct resync_agent {
	char *rag_hostname;
	char *rag_mountpoint;
	int rag_maxjobs;
	int rag_jobs;
	int rag_bad;
	struct list_head rag_list;
};

struct resync_job {
	struct list_head rj_list;
	struct lu_fid rj_fid;
	struct resync_agent *rj_agent;
	__u64 rj_index;
	int rj_pid;
	unsigned long start;
};

extern char **environ;

enum amigo_resync_type {
	AMIGO_RESYNC_NONE = 0,
	AMIGO_RESYNC_EXTEND = 1,
	AMIGO_RESYNC_RESYNC = 2
};

static int lamigo_spawn_replication(struct resync_job *rj, int resync)
{
	struct resync_agent *a, *tmp;
	char cmd[PATH_MAX * 2];
	char *argv[] = {"sh", "-c", cmd, NULL };
	int pid, jobs;

	/* choose the agent - for simplicity just one with minimal jobs */
	/* XXX: to support many agents it makes sense to introduce additional
	 * structure like a sorted list or something similar */
	/* XXX: take locality into account */
	jobs = 1 << 30;
	a = NULL;
	list_for_each_entry(tmp, &lamigo_agent_list, rag_list) {
		if (tmp->rag_bad)
			continue;
		if (tmp->rag_jobs >= tmp->rag_maxjobs)
			continue;
		if (tmp->rag_jobs < jobs) {
			a = tmp;
			jobs = tmp->rag_jobs;
		}
	}
	if (!a) {
		fprintf(stderr, "no good agent\n");
		return -EBUSY;
	}
	rj->rj_agent = a;
	rj->start = time(NULL);

	DEBUG("new job %s for "DFID" spawned on %s\n",
		resync == AMIGO_RESYNC_EXTEND ? "extend" : "resync",
		PFID(&rj->rj_fid), rj->rj_agent->rag_hostname);

	pid = fork();
	if (pid > 0) {
		a->rag_jobs++;
		stats.s_spawned++;
		return pid;
	}
	if (pid < 0)
		return pid;

	if (resync == AMIGO_RESYNC_EXTEND) {
		snprintf(cmd, sizeof(cmd),
			 "%s %s lfs mirror extend -N -p %s %s/.lustre/fid/"DFID,
			 ssh, a->rag_hostname, opt.o_tgt_pool,
			 a->rag_mountpoint, PFID(&rj->rj_fid));
	} else if (resync == AMIGO_RESYNC_RESYNC) {
		snprintf(cmd, sizeof(cmd),
			 "%s %s lfs mirror resync %s/.lustre/fid/"DFID,
			 ssh, a->rag_hostname, a->rag_mountpoint,
			 PFID(&rj->rj_fid));
	} else {
		fprintf(stderr, "unknown resync: %d\n", resync);
	}

	close(0);
	close(1);
	close(2);

	/* use debug file for ssh/lfs stderr if defined */
	if (dfile) {
		dup2(fileno(dfile), 1);
		dup2(fileno(dfile), 2);
	} else {
		int fd;
		fd = open("/dev/null", O_WRONLY);
		if (fd >= 0) {
			dup2(fd, 1);
			dup2(fd, 2);
		}
	}

	pid = execve("/bin/sh", &argv[0], environ);
	return pid;
}

static inline struct lov_user_md *
lov_comp_entry(struct lov_comp_md_v1 *comp_v1, int ent_idx)
{
	return (struct lov_user_md *)((char *)comp_v1 +
			comp_v1->lcm_entries[ent_idx].lcme_offset);
}

static bool lamigo_entry_needs_resync(struct lov_comp_md_entry_v1 *entry)
{
	if (entry->lcme_flags & LCME_FL_NOSYNC)
		return false;

	if (entry->lcme_flags & LCME_FL_STALE)
		return true;

	return false;
}

char *lmm_magic_to_str(unsigned long magic)
{
	if (magic == LOV_USER_MAGIC_V1)
		return "V1";
	else if (magic == LOV_USER_MAGIC_V3)
		return "V3";
	else if (magic == LOV_USER_MAGIC_COMP_V1)
		return "C1";
	return "V??";
}


static int lamigo_is_in_sync(struct lu_fid *fid, __u64 crtime)
{
	struct lov_comp_md_entry_v1 *entry;
	struct lov_user_mds_data *lmdp;
	struct lov_comp_md_v1 *comp_v1;
	struct lov_user_md_v3 *v3 = NULL;
	enum amigo_resync_type resync;
	int i, rc, onsrc, ontgt;
	char lmd[8192];
	lstat_t *st;

	snprintf(lmd, sizeof(lmd), DFID, PFID(fid));
	/* XXX: to be fixed after https://review.whamcloud.com/#/c/35167/ */
	rc = ioctl(dirfd(dotlustre), IOC_MDC_GETFILEINFO, lmd);
	if (rc) {
		if (rc != -ENOENT)
			DEBUG("GET_LMD_INFO for "DFID": %d\n", PFID(fid), rc);
		return AMIGO_RESYNC_NONE;
	}

	lmdp = (void *)lmd;
	st = &lmdp->lmd_st;

	/* replicate only regular files */
	if (!S_ISREG(st->st_mode)) {
		resync = AMIGO_RESYNC_NONE;
		goto out;
	}

	/* XXX: check it hasn't been touched since this record, not sure how this can
	 * be done though as the file's timestamps are taken from the client side
	 * while changelog's timestamp is generated by the server and those can
	 * be out of sync
	 */

	/*
	 * extra rules can be implemented here to filter files by size, credential,
	 * short-name, jobid, etc
	 */

	v3 = (void *)&lmdp->lmd_lmm;
	if (v3->lmm_magic == LOV_USER_MAGIC_V1) {
		/* XXX: there is no pool information, so we have no idea
		 * whether we need to replicatet this file.
		 * the next version will be tracking OST<->pool mapping
		 * and check for specific OSTs */
		resync = AMIGO_RESYNC_NONE;
		goto out;
	}

	if (v3->lmm_magic == LOV_USER_MAGIC_V3) {
		/* do replication only for specific pool */
		if (strncmp(v3->lmm_pool_name, opt.o_src_pool,
			    opt.o_src_pool_len) == 0) {
			resync = AMIGO_RESYNC_EXTEND;
			goto out;
		}
		resync = AMIGO_RESYNC_NONE;
		goto out;
	}

	if (v3->lmm_magic != LOV_USER_MAGIC_COMP_V1) {
		resync = AMIGO_RESYNC_NONE;
		goto out;
	}

	comp_v1 = (void *)&lmdp->lmd_lmm;

	onsrc = 0;
	ontgt = 0;
	resync = AMIGO_RESYNC_NONE;
	for (i = 0; i < comp_v1->lcm_entry_count; i++) {
		entry = &comp_v1->lcm_entries[i];
		v3 = (struct lov_user_md_v3 *)lov_comp_entry(comp_v1, i);
		if (v3->lmm_magic == LOV_USER_MAGIC_V3 &&
		    !strncmp(v3->lmm_pool_name, opt.o_src_pool,
			     opt.o_src_pool_len)) {
			/* this is source */
			onsrc = 1;
			if (lamigo_entry_needs_resync(entry))
				resync = AMIGO_RESYNC_RESYNC;
		} else if (v3->lmm_magic == LOV_USER_MAGIC_V3 &&
		    !strncmp(v3->lmm_pool_name, opt.o_tgt_pool,
			     opt.o_tgt_pool_len)) {
			/* there is a replica on the target */
			ontgt = 1;
			if (lamigo_entry_needs_resync(entry))
				resync = AMIGO_RESYNC_RESYNC;
		} else {
			/* this is replica */
			if (lamigo_entry_needs_resync(entry))
				resync = AMIGO_RESYNC_RESYNC;
		}
	}
	if (!onsrc) {
		/* ignore files not presented on the source pool */
		resync = AMIGO_RESYNC_NONE;
	} else if (!ontgt) {
		/* found on the source pool, but not replicated
		 * to the target pool yet */
		resync = AMIGO_RESYNC_EXTEND;
	}

out:
	v3 = (void *)&lmdp->lmd_lmm;
	DEBUG("check "DFID" with magic=%s: %d\n", PFID(fid),
		v3 ? lmm_magic_to_str(v3->lmm_magic) : "", resync);

	return resync;
}

static int lamigo_update_one(struct fid_rec *f)
{
	int resync;
	struct resync_job *rj;

	/* check for a mirror */
	if ((resync = lamigo_is_in_sync(&f->fr_fid, f->fr_time)) == AMIGO_RESYNC_NONE) {
		/* nothing to do */
		return 0;
	}

	if (lamigo_jobs_running >= lamigo_max_jobs) {
		/* all the agents are busy */
		return 1;
	}

	rj = malloc(sizeof(struct resync_job));
	if (rj == NULL) {
		DEBUG("can't allocate memory for a job\n");
		return 1;
	}
	rj->rj_fid = f->fr_fid;
	rj->rj_index = f->fr_index;
	rj->rj_pid = lamigo_spawn_replication(rj, resync);
	if (rj->rj_pid < 0) {
		DEBUG("can't fork a job: rc=%d\n", rj->rj_pid);
		free(rj);
		return 1;
	}
	/* this is safe as lamigo is single-threaded,
	 * so we can't see job completion by this moment */
	lamigo_jobs_running++;
	list_add_tail(&rj->rj_list, &lamigo_job_list);

	return 0;
}

static int lamigo_start_update(int count)
{
	int rc = 0;
	int i = 0;

	while (i < count) {
		struct fid_rec *f;

		f = list_entry(head.lh_list.next, struct fid_rec, fr_link);
		rc = lamigo_update_one(f);
		if (rc)
			break;
		if (rc == 0) {
			list_del_init(&f->fr_link);
			fid_hash_del(f);
			free(f);
			head.lh_cached_count--;
			i++;
		}
	}

	return rc;
}

static int lamigo_check_sync(void)
{
	int rc = 0;
	int count;

repeated:
	count = 0;
	if (list_empty(&head.lh_list))
		return 0;

	if (head.lh_cached_count > opt.o_cached_fid_hiwm)
		count = opt.o_batch_sync_cnt;
	else {
		struct fid_rec *f;
		time_t now;

		/* When the first record in the list was not being
		 * processed for a long time (more than o_min_age),
		 * pop the record, start to handle it immediately.
		 */
		now = time(NULL);
		f = list_entry(head.lh_list.next, struct fid_rec, fr_link);
		if (now > ((f->fr_time >> 30) + opt.o_min_age))
			count = 1;
	}

	if (count > 0)
		rc = lamigo_start_update(count);

	if (rc == 0 && count == 1)
		goto repeated;

	return rc;
}

static int lamigo_process_record(struct changelog_rec *rec)
{
	__u64 index = rec->cr_index;
	struct fid_rec *f;
	int rc = 0;

	lamigo_last_processed_idx = rec->cr_index;

	stats.s_read++;

	if (rec->cr_type == CL_UNLINK) {
		if ((rec->cr_flags & CLF_UNLINK_LAST) == 0)
			goto skip;
		/* no need to replicate removed files */
		f = fid_hash_find(&rec->cr_tfid);
		if (f) {
			list_del_init(&f->fr_link);
			fid_hash_del(f);
			free(f);
			head.lh_cached_count--;
			stats.s_removed++;
		}
		return 0;

	}

	if (rec->cr_type != CL_CLOSE)
		goto skip;

	stats.s_processed++;

	f = fid_hash_find(&rec->cr_tfid);
	if (f == NULL) {
		f = malloc(sizeof(struct fid_rec));
		if (f == NULL) {
			rc = -ENOMEM;
			llapi_error(LLAPI_MSG_ERROR, rc,
					"failed to alloc memory for fid_rec");
			return rc;
		}

		f->fr_fid = rec->cr_tfid;
		f->fr_index = index;
		f->fr_time = rec->cr_time;
		INIT_HLIST_NODE(&f->fr_node);
		fid_hash_add(f);
		/*
		 * The newly changelog record index is processed in the
		 * ascending order, so it is safe to put the record at
		 * the tail of the ordered list.
		 */
		list_add_tail(&f->fr_link, &head.lh_list);
		head.lh_cached_count++;
	} else {
		stats.s_dups++;
		f->fr_index = index;
		f->fr_time = rec->cr_time;
		/* move to the tail, it doesn't need to be strictly sorted */
		list_del(&f->fr_link);
		list_add_tail(&f->fr_link, &head.lh_list);
	}

	return rc;

skip:
	stats.s_skipped++;
	return 0;
}

static unsigned long get_fid_cache_size(int pct)
{
	struct sysinfo sinfo;
	unsigned long cache_size;
	int rc;

	rc = sysinfo(&sinfo);
	if (rc) {
		llapi_error(LLAPI_MSG_ERROR, rc, "failed to get sysinfo");
		/* ignore this error, just pick some reasonable static
		 * limit for the cache size (e.g. 256MB, default value).
		 */
		cache_size = DEF_CACHE_SIZE;
	} else {
		/* maximum cached fid size is tunned according to total
		 * memory size, e.g. 5% of the memroy.
		 */
		cache_size = sinfo.totalram * pct / 100;
	}

	return cache_size;
}

static time_t lamigo_last_cleared = 0;
static __u64 lamigo_last_cleared_index = 0;

static void lamigo_check_and_clear_changelog(void)
{
	struct resync_job *tmp;
	__u64 index;
	int rc;

	/* find minimal record we're still working with */

	index = lamigo_last_processed_idx;

	/* this checks prevents too frequent in-memory-search for
	 * the minimal record-in-work: every 256th record and not
	 * frequently than once a 5 seconds */
	if ((index & 256) == 0 && time(NULL) - lamigo_last_cleared < 5)
		return;
	lamigo_last_cleared = time(NULL);

	if (!list_empty(&head.lh_list)) {
		struct fid_rec *f;
		f = list_entry(head.lh_list.next, struct fid_rec, fr_link);
		if (f->fr_index < index)
			index = f->fr_index;
	}

	list_for_each_entry(tmp, &lamigo_job_list, rj_list) {
		if (tmp->rj_index < index)
			index = tmp->rj_index;
	}

	if (index - lamigo_last_cleared_index < opt.o_chlg_clear_frequency)
		return;

	DEBUG("CLEAR upto %llu in %s (%llu last seen)\n", index,
		opt.o_chlg_user, lamigo_last_processed_idx);
	lamigo_last_cleared_index = index;
	rc = llapi_changelog_clear(opt.o_mdtname, opt.o_chlg_user, index);
	if (rc)
		llapi_error(LLAPI_MSG_ERROR, rc,
			    "failed to clear changelog record: %s:%llu",
			    opt.o_chlg_user, index);
}

static void lamigo_check_jobs(void)
{
	struct resync_job *rj = NULL, *tmp;
	int rc, status;

	/* check spawned processes */
	rc = waitpid(-1, &status, WNOHANG);
	// rc == -1 + errno=-EINTR -- interrupted by a signal
	// rc == 0 - nobody exited
	// rc > 0 - pid of child exited
	if (rc < 0 && errno == ECHILD)
		return;

	if (rc == 0) {
		/* some of child has changed status, but still alive */
		return;
	}
	if (rc < 0) {
		DEBUG("CHECK JOB: %d\n", errno);
		return;
	}

	list_for_each_entry(tmp, &lamigo_job_list, rj_list) {
		if (tmp->rj_pid == rc) {
			rj = tmp;
			break;
		}
	}
	if (rj == NULL) {
		fprintf(stderr, "unknown job %d exited\n", rc);
		return;
	}

	if (!WIFEXITED(status)) {
		/* the job was terminated abnormally */
		/* XXX: do not cancel llog record? reschedule */
		fprintf(stderr, "unknown job %d terminated\n", rc);
		return;
	}

	rc = WEXITSTATUS(status);
	DEBUG("job %d on "DFID" completed in %lu: %d\n",
		rj->rj_pid, PFID(&rj->rj_fid),
		time(NULL) - rj->start, rc);
	if (rc == EBUSY) {
		/* the file was busy, there will be another CLOSE
		 * in the changelog, we can just cancel our record */
	} else if (rc == 255) {
		/* couldn't connect */
		/* XXX: resubmit the job */
	} else if (rc == 127) {
		/* likely invalid setup on the agent (missing lfs?) */
		/* XXX: resubmit the job */
	} else if (rc == ENODATA) {
		/* no striping info or file disappeared */
		/* no need to handle, forget the changelog record */
	}

	assert(rj->rj_agent->rag_jobs > 0);
	rj->rj_agent->rag_jobs--;
	list_del(&rj->rj_list);
	free(rj);
	lamigo_jobs_running--;
}

static void lamigo_add_agent(const char *host, const char *mnt, char *jobs)
{
	struct resync_agent *a;

	a = malloc(sizeof(*a));
	if (!a) {
		fprintf(stderr, "can't allocate memory for agent\n");
		exit(1);
	}

	a->rag_hostname = strdup(host);
	a->rag_mountpoint = strdup(mnt);
	a->rag_maxjobs = jobs ? atoi(jobs) : 8;
	list_add(&a->rag_list, &lamigo_agent_list);

	a->rag_jobs = 0;
	a->rag_bad = 0;
	lamigo_max_jobs += a->rag_maxjobs;

	DEBUG("AGENT: %s %s %d\n", a->rag_hostname,
		a->rag_mountpoint, a->rag_maxjobs);
}

static struct option options[] = {
	{ "config", required_argument, NULL, 'f' },
	{ "mdt", required_argument, NULL, 'm' },
	{ "user", required_argument, 0, 'u'},
	{ "daemonize", no_argument, NULL, 'd'},
	{ "min-age", required_argument, NULL, 'a'},
	{ "max-cache", required_argument, NULL, 'c'},
	{ "agent", required_argument, NULL, 'g'},
	{ "src", required_argument, NULL, 's'},
	{ "tgt", required_argument, NULL, 't'},
	{ "dump", required_argument, NULL, 'w'},
	{ "verbose", no_argument, NULL, 'v'},
	{ "help", no_argument, NULL, 'h' },
	{ NULL }
};

void load_config(char *name);

void lamigo_process_opt(int c, char *optarg)
{
	int rc;

	switch (c) {
		char *host, *mnt, *jobs;
	default:
		rc = -EINVAL;
		llapi_error(LLAPI_MSG_ERROR, rc,
			    "%s: unknown option '-%c'\n",
			    prog, optopt);
		usage();
		break;
	case 'g':
		host = strsep(&optarg, ":");
		mnt = strsep(&optarg, ":");
		jobs = strsep(&optarg, ":");
		if (!host || !mnt) {
			fprintf(stderr, "invalid agent definition\n");
			exit(1);
		}
		lamigo_add_agent(host, mnt, jobs);
		break;
	case 'u':
		opt.o_chlg_user = strdup(optarg);
		break;
	case 's':
		opt.o_src_pool = strdup(optarg);
		break;
	case 't':
		opt.o_tgt_pool = strdup(optarg);
		break;
	case 'h':
		usage();
		break;
	case 'm':
		opt.o_mdtname = strdup(optarg);
		break;
	case 'd':
		opt.o_daemonize = true;
		break;
	case 'a':
		opt.o_min_age = atoi(optarg);
		if (opt.o_min_age < 0) {
			rc = -EINVAL;
			llapi_error(LLAPI_MSG_ERROR, rc,
				    "bad value for -a %s", optarg);
			usage();
		}
		break;
	case 'c':
		rc = Parser_size(&cache_size, optarg);
		if (rc < 0) {
			rc = -EINVAL;
			llapi_error(LLAPI_MSG_ERROR, rc,
				    "bad value for -c '%s'", optarg);
			usage();
		}

		/* For value < 100, it is taken as the percentage of
		 * total memory instead.
		 */
		if (cache_size < 100)
			cache_size = get_fid_cache_size(cache_size);
		llapi_printf(LLAPI_MSG_INFO, "Cache size: %lu\n",
			     cache_size);
		break;
	case 'f':
		opt.o_daemonize = true;
		load_config(optarg);
		break;
	case 'w':
		opt.o_dump_file = strdup(optarg);
		break;
	case 'b':
		if (strcmp(optarg, "-") == 0)
			dfile = stderr;
		else
			dfile = fopen(optarg, "a+");
		break;
	case 'v':
		opt.o_verbose++;
		break;
	}
}

static struct option *lamigo_keyword_lookup(const char *keyword)
{
	int i = 0;
	while (options[i].name) {
		if (strcmp(keyword, options[i].name) == 0)
			return options + i;
		i++;
	}
	return NULL;
}

void load_config(char *name)
{
	char buf[PATH_MAX];
	FILE *f;

	f = fopen(name, "r");
	if (!f) {
		fprintf(stderr, "can't open config file %s\n", name);
		exit(1);
	}
	while (!feof(f)) {
		struct option *opt;
		char *s, *t;
		if (!fgets(buf, sizeof(buf), f))
			break;
		s = buf;
		while (*s == ' ' && *s != 0)
			s++;
		if (*s == '#')
			continue;
		t = strsep(&s, "=\n");
		if (!t || *t == 0)
			continue;
		opt = lamigo_keyword_lookup(t);
		if (!opt) {
			fprintf(stderr, "unknown tunable: %s\n", t);
			continue;
		}
		if (opt->val == 'f') {
			/* you shall not pass! */
			continue;
		}
		if (opt->has_arg == required_argument) {
			optarg = strsep(&s, "\n");
			if (!optarg) {
				fprintf(stderr, "no argument for %s\n", t);
				exit(1);
			}
		} else {
			optarg = NULL;
		}
		DEBUG("CONFIG: %s %s\n", t, optarg);
		lamigo_process_opt(opt->val, optarg);
	}

	fclose(f);
}

void lamigo_parse_opts(int argc, char **argv)
{
	char fsname[MAX_OBD_NAME + 1];
	int rc, c;

	while ((c = getopt_long(argc, argv, "p:u:hm:di:a:c:vg:s:t:f:w:b:", options, NULL))
	       != EOF) {
		lamigo_process_opt(c, optarg);

	}

	if (argc != optind + 1) {
		llapi_err_noerrno(LLAPI_MSG_ERROR,
				  "%s: no mount point specified\n", argv[0]);
		usage();
	}

	opt.o_mntpt = argv[optind];
	rc = llapi_search_fsname(opt.o_mntpt, fsname);
	if (rc < 0) {
		llapi_error(LLAPI_MSG_ERROR, rc,
			    "cannot find a Lustre file system mounted at '%s'",
			    opt.o_mntpt);
		usage();
	}

	if (!opt.o_mdtname)
		usage();

	if (!opt.o_chlg_user)
		usage();

	snprintf(fsname, sizeof(fsname), "%s/.lustre/fid", opt.o_mntpt);
	dotlustre = opendir(fsname);
	if (!dotlustre) {
		fprintf(stderr, "can't open '%s': %d\n", fsname, errno);
		exit(1);
	}

	if (lamigo_max_jobs < 1) {
		llapi_err_noerrno(LLAPI_MSG_ERROR, "no agents specified?\n");
		usage();
	}

	if (opt.o_src_pool == NULL) {
		fprintf(stderr, "source pool should be specified\n");
		usage();
	}
	opt.o_src_pool_len = strlen(opt.o_src_pool);

	if (opt.o_tgt_pool == NULL) {
		fprintf(stderr, "target pool should be specified\n");
		usage();
	}
	opt.o_tgt_pool_len = strlen(opt.o_tgt_pool);

	if (opt.o_daemonize) {
		rc = daemon(1, 1);
		if (rc < 0) {
			rc = -errno;
			llapi_error(LLAPI_MSG_ERROR, rc, "cannot daemonize");
			usage();
		}

		setbuf(stdout, NULL);
	}

	opt.o_cached_fid_hiwm = cache_size / sizeof(struct fid_rec);
	opt.o_batch_sync_cnt = opt.o_cached_fid_hiwm / 2;
}

int main(int argc, char **argv)
{
	struct changelog_rec	*rec;
	int	 rc;
	void	*chglog_hdlr;
	bool	 stop = 0;
	int	 ret = 0;

	prog = argv[0];
	rc = lamigo_setup();
	if (rc < 0)
		return rc;
	lamigo_parse_opts(argc, argv);

	llapi_printf(LLAPI_MSG_DEBUG, "Start receiving records\n");
	rc = llapi_changelog_start(&chglog_hdlr,
				   CHANGELOG_FLAG_FOLLOW |
				   CHANGELOG_FLAG_BLOCK |
				   CHANGELOG_FLAG_JOBID |
				   CHANGELOG_FLAG_EXTRA_FLAGS,
				   opt.o_mdtname, 0);
	if (rc) {
		/* XXX: probably keep trying in some cases? */
		llapi_error(LLAPI_MSG_ERROR, rc,
				"unable to open changelog of MDT [%s]\n",
				opt.o_mdtname);
		return rc;
	}

	while (!stop) {
		struct pollfd fds[1];

		fds[0].fd = llapi_changelog_get_fd(chglog_hdlr);
		fds[0].events = POLLIN;

		rc = poll(fds, 1, 3 * 1000);
		if (rc <= 0)
			goto next;

		do {
			rc = llapi_changelog_recv(chglog_hdlr, &rec);
			if (!rc) {
				rc = lamigo_process_record(rec);
				if (rc) {
					llapi_error(LLAPI_MSG_ERROR, rc,
							"failed to process record");
					ret = rc;
				}
				llapi_changelog_free(&rec);
			}
			/* keep going through all the records in a single buffer
			 * then interrupt to handle spawed jobs, expired records */
		} while (llapi_changelog_in_buf(chglog_hdlr) && rc == 0);

next:
		rc = lamigo_check_sync();
		if (rc < 0) {
			stop = true;
			ret = rc;
		}
		lamigo_check_jobs();
		lamigo_check_and_clear_changelog();
	}

	/* wait for all jobs to complete */
	while (lamigo_jobs_running) {
		sleep(1);
		lamigo_check_jobs();
		lamigo_check_and_clear_changelog();
	}

	rc = llapi_changelog_fini(&chglog_hdlr);
	if (rc) {
		llapi_error(LLAPI_MSG_ERROR, rc,
				"unable to close changelog of MDT [%s]",
				opt.o_mdtname);
		ret = rc;
		return rc;
	}

	lamigo_cleanup();
	return ret;
}
