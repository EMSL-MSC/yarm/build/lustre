#include <unistd.h>
#include <pthread.h>
#include <lustre/lustreapi.h>
#include <ext2fs/ext2fs.h>
#include <uapi/linux/lustre/lustre_fid.h>
#include "lipe_policy.h"

#ifdef HAVE_ZFS
#include "lipe_zfs.h"
#endif

#ifdef HAVE_LUSTRE_PFL
/*
 * Look into layout to check whether the file has dom.
 *
 * Please check mdt_lmm_dom_entry() in Lustre for more information
 *
 */
enum lustre_dom_status dom_status_from_lum(struct lov_user_md *lum)
{
	int i, j, ent_count;
	struct lov_user_md *md;
	struct lov_comp_md_v1 *comp;
	struct lov_comp_md_entry_v1 *ent;
	struct lov_user_ost_data_v1 *ost;

	if (lum->lmm_magic != LOV_USER_MAGIC_COMP_V1)
		return LDS_NO_DOM;

	comp = (struct lov_comp_md_v1 *)lum;
	ent_count = comp->lcm_entry_count;

	for (i = 0; i < ent_count; i++) {
		ent = &comp->lcm_entries[i];
		md = (struct lov_user_md *)((char *)comp +
			ent->lcme_offset);
		for (j = 0; j < md->lmm_stripe_count; j++) {
			ost = &md->lmm_objects[j];
			/* if there is any object on OST */
			if (ost->l_ost_idx != (__u32)-1UL)
				return LDS_DOM_OST;
		}
	}
	return LDS_DOM_ONLY;
}
#else
enum lustre_dom_status dom_status_from_lum(struct lov_user_md *lum)
{
	return LDS_UNKNOWN;
}
#endif
int get_link_ea(struct lipe_object *object, struct lipe_policy_attrs *attrs)
{
	int rc;

	if (object->lo_backfs_ops == NULL ||
	    object->lo_backfs_ops->get_link_ea == NULL)
		return -ENOTSUP;

	rc = object->lo_backfs_ops->get_link_ea(object, attrs);
	if (rc) {
		LDEBUG("failed to get LINKEA of object [%llu]\n",
		       object->lo_id);
		return rc;
	}
	return 0;
}

int get_lma_ea(struct lipe_object *object, struct lipe_policy_attrs *attrs)
{
	int rc;

	if (object->lo_backfs_ops == NULL ||
	    object->lo_backfs_ops->get_lma_ea == NULL)
		return -ENOTSUP;

	rc = object->lo_backfs_ops->get_lma_ea(object, attrs);
	if (rc) {
		/*LDEBUG("failed to get LMA of object [%llu]\n",
		       object->lo_id);*/
		return rc;
	}
	return 0;
}

int get_fid_ea(struct lipe_object *object, struct lipe_policy_attrs *attrs)
{
	int rc;

	if (object->lo_backfs_ops == NULL ||
	    object->lo_backfs_ops->get_fid_ea == NULL)
		return -ENOTSUP;

	rc = object->lo_backfs_ops->get_fid_ea(object, attrs);
	if (rc) {
		/*LDEBUG("failed to get FID of object [%llu]\n",
		       object->lo_id);*/
		return rc;
	}
	return 0;
}

int get_hsm_ea(struct lipe_object *object, struct lipe_policy_attrs *attrs,
		      bool watch)
{
	int rc;

	if (object->lo_backfs_ops == NULL ||
	    object->lo_backfs_ops->get_hsm_ea == NULL)
		return -ENOTSUP;

	rc = object->lo_backfs_ops->get_hsm_ea(object, attrs, watch);
	if (rc) {
		LDEBUG("failed to get HSM of object [%llu]\n",
		       object->lo_id);
		return rc;
	}
	return 0;
}

static int get_lum_ea(struct lipe_object *object,
		      struct lipe_policy_attrs *attrs,
		      bool watch)
{
	int rc;

	if (object->lo_backfs_ops == NULL ||
	    object->lo_backfs_ops->get_lum_ea == NULL)
		return -ENOTSUP;

	rc = object->lo_backfs_ops->get_lum_ea(object, attrs, watch);
	if (rc) {
		LDEBUG("failed to get LUM of object [%llu]\n",
		       object->lo_id);
		return rc;
	}
	return 0;
}

/*
 * Do not use static for this function, since build will fail if Lustre has no
 * SOM support
 */
int get_som_ea(struct lipe_object *object,
	       struct lipe_policy_attrs *attrs,
	       bool watch)
{
	int rc;

	if (object->lo_backfs_ops == NULL ||
	    object->lo_backfs_ops->get_som_ea == NULL)
		return -ENOTSUP;

	rc = object->lo_backfs_ops->get_som_ea(object, attrs, watch);
	if (rc) {
		LDEBUG("failed to get SoM of object [%llu]\n",
		       object->lo_id);
		return rc;
	}
	return 0;
}

static int get_dom_status(struct lipe_object *object,
			  struct lipe_policy_attrs *attrs,
			  bool watch)
{
	int rc;

	if (!(attrs->lpa_attr_bits & LIPE_POLICY_BIT_LOVEA)) {
		rc = get_lum_ea(object, attrs, watch);
		if (rc) {
			LDEBUG("failed to get LOVEA of object [%llu]\n",
			       object->lo_id);
			return rc;
		}
	}

	rc = dom_status_from_lum(attrs->lpa_lum);
	if (rc == LDS_UNKNOWN)
		return -ENOTSUP;
	attrs->lpa_dom_status = rc;
	attrs->lpa_attr_bits |= LIPE_POLICY_BIT_DOM;

	/* lpa_size and lpa_blocks should be the accurate values */
	if (rc == LDS_DOM_ONLY)
		attrs->lpa_attr_bits |= LIPE_POLICY_BIT_SIZE;
	return 0;
}

int get_size(struct lipe_object *object, struct lipe_policy_attrs *attrs,
		    bool watch)
{
	int rc;
#ifdef HAVE_LAZY_SIZE_ON_MDT
	struct lustre_som_attrs	*som = &attrs->lpa_som;
#endif

	//LASSERT(attrs->lpa_attr_bits & LIPE_POLICY_BIT_ATTR);
	if ((attrs->lpa_mode & S_IFMT) != S_IFREG) {
		/* If file is not regular file, the size is valid */
		attrs->lpa_attr_bits |= LIPE_POLICY_BIT_SIZE;
		return 0;
	}

	rc = get_dom_status(object, attrs, watch);
	if (rc)
		return rc;

	if (attrs->lpa_attr_bits & LIPE_POLICY_BIT_SIZE)
		return 0;

#ifdef HAVE_LAZY_SIZE_ON_MDT
	if (!(attrs->lpa_attr_bits & LIPE_POLICY_BIT_SOM)) {
		rc = get_som_ea(object, attrs, watch);
		if (rc)
			return rc;
	}

	/* Take the estimated size/blocks of SoM as the size/blocks */
	assert(attrs->lpa_attr_bits & LIPE_POLICY_BIT_SOM);
	/* This should never happen, but check anyway */
	if (som->lsa_valid == SOM_FL_UNKNOWN)
		return -ENOTSUP;
	attrs->lpa_size = som->lsa_size;
	attrs->lpa_blocks = som->lsa_blocks;
	attrs->lpa_attr_bits |= LIPE_POLICY_BIT_SIZE;
	return 0;
#else /* !HAVE_LAZY_SIZE_ON_MDT */
	return -ENOTSUP;
#endif /* !HAVE_LAZY_SIZE_ON_MDT */
}

#if 0
static int lipe_rule_read_attrs(struct lipe_object *object,
				struct lipe_rule *rule,
				struct lipe_policy_attrs *attrs,
				struct lipe_policy_sysattrs *sysattrs,
				struct lu_fid *watch_fid,
				bool need_fid)
{
	int rc;
	bool watch = false;

	if (rule->lr_attr_bits & LIPE_POLICY_BIT_LINKEA &&
	    (!(attrs->lpa_attr_bits & LIPE_POLICY_BIT_LINKEA))) {
		rc = get_link_ea(object, attrs);
		if (rc) {
			LDEBUG("failed to get LINKEA of object [%llu]\n",
			       object->lo_id);
			return rc;
		}
	}

	if ((!(attrs->lpa_attr_bits & LIPE_POLICY_BIT_LMAEA)) &&
	    ((rule->lr_attr_bits & LIPE_POLICY_BIT_LMAEA) ||
	     fid_is_sane(watch_fid) || need_fid)) {
		rc = get_lma_ea(object, attrs);
		if (rc) {
			LDEBUG("failed to get LMAEA of object [%llu]\n",
			       object->lo_id);
			return rc;
		}
		if (lu_fid_eq(&attrs->lpa_fid, watch_fid))
			watch = true;
	}

	if ((rule->lr_attr_bits & LIPE_POLICY_BIT_HSMEA) &&
	    (!(attrs->lpa_attr_bits & LIPE_POLICY_BIT_HSMEA))) {
		rc = get_hsm_ea(object, attrs, watch);
		if (rc) {
			LDEBUG("failed to get HSMEA of object [%llu]\n",
			       object->lo_id);
			return rc;
		}
	}

	if ((rule->lr_attr_bits & LIPE_POLICY_BIT_LOVEA) &&
	    (!(attrs->lpa_attr_bits & LIPE_POLICY_BIT_LOVEA))) {
		rc = get_lum_ea(object, attrs, watch);
		if (rc) {
			LDEBUG("failed to get LOVEA of object [%llu]\n",
			       object->lo_id);
			return rc;
		}
	}

	if ((rule->lr_attr_bits & LIPE_POLICY_BIT_DOM) &&
	    (!(attrs->lpa_attr_bits & LIPE_POLICY_BIT_DOM))) {
		rc = get_dom_status(object, attrs, watch);
		if (rc) {
			LDEBUG("failed to get DoM status of object [%llu]\n",
			       object->lo_id);
			return rc;
		}
	}

	if ((rule->lr_attr_bits & LIPE_POLICY_BIT_SIZE) &&
	    (!(attrs->lpa_attr_bits & LIPE_POLICY_BIT_SIZE))) {
		rc = get_size(object, attrs, watch);
		if (rc) {
			LDEBUG("failed to get size of object [%llu]\n",
			       object->lo_id);
			return rc;
		}
	}
	return 0;
}
#endif

int lipe_policy_apply(struct lipe_object *object,
		      struct lipe_policy *policy,
		      struct lipe_instance *instance,
		      struct lipe_policy_attrs *attrs,
		      struct lipe_policy_sysattrs *sysattrs,
		      struct scan_result *scan_result,
		      const char *srv_name,
		      bool abort_failure,
		      struct counter_list *counter_list,
		      struct classify_list *classify_list,
		      struct lu_fid *watch_fid,
		      bool all_object,
		      bool lustre)
{
	int rc;

	/* the object has to have LMA (storing FID) */
	rc = get_lma_ea(object, attrs);
	if (rc)
		return 0;
	if (!fid_is_norm(&attrs->lpa_fid) && !fid_is_idif(&attrs->lpa_fid))
		return 0;

	/* .. and FILTER FID storing file FID (object on MDS) */
	get_fid_ea(object, attrs);

	if (instance->li_callback)
		rc = instance->li_callback(instance, object, attrs);

	return rc;
}

struct lipe_instance *lipe_instance_alloc(int group_number, const char *name)
{
	struct lipe_instance *instance;

	instance = malloc(sizeof(*instance));
	if (instance == NULL)
		return NULL;

	strncpy(instance->li_device, name, sizeof(instance->li_device) - 1);

	instance->li_groups = malloc(sizeof(*instance->li_groups) * group_number);
	if (instance->li_groups == NULL)
		goto out;

	instance->li_group_number = group_number;
	return instance;
out:
	free(instance);
	return NULL;
}

int scan_threads_abort(struct thread_info *infos, int num_threads)
{
	int			 rc;
	int			 ret = 0;
	int			 i;
	struct thread_info	*pinfo;

	for (i = 0; i < num_threads; i++) {
		pinfo = &infos[i];
		if (!pinfo->ti_started)
			continue;
		pinfo->ti_stopping = true;
	}

	for (i = 0; i < num_threads; i++) {
		pinfo = &infos[i];
		if (!pinfo->ti_started)
			continue;
		rc = pthread_join(infos[i].ti_thread_id, NULL);
		if (rc) {
			LERROR("failed to join thread\n");
			if (ret == 0)
				ret = rc;
		}
		//counter_list_fini(&pinfo->ti_counter_list);
		//classify_list_fini(&pinfo->ti_classify_list);
	}

	for (i = 0; i < num_threads; i++) {
		pinfo = &infos[i];
		if (pinfo->private != NULL) {
			free(pinfo->private);
			pinfo->private = NULL;
		}
	}
	free(infos);

	return ret;
}

int scan_threads_start(struct thread_info **pinfo, int num_threads,
		       struct lipe_instance *instance,
		       struct lipe_policy *policy,
		       struct global_info *global_info,
		       enum lipe_backfs_type backfs_type)
{
	int			 rc;
	int			 ret;
	int			 i;
	struct thread_info	*infos;
	struct thread_info	*tmp_pinfo;
	//const char		*workspace;
	pthread_attr_t		 attr;
#ifdef HAVE_ZFS
	objset_t		*os;
	dnode_t			*dn;
	uint64_t		 max_offset;
	uint64_t		 max_object;
	uint64_t		 start_index[num_threads];
	uint64_t		 end_index[num_threads];
	uint64_t		 left, avg;
	struct zfs_obj_range	*zor[num_threads];
	pthread_t		 tid;
#endif

	//workspace = global_info->gi_workspace;
	rc = pthread_attr_init(&attr);
	if (rc) {
		LERROR("failed to set pthread attribute\n");
		return rc;
	}

	infos = calloc(num_threads, sizeof(struct thread_info));
	if (infos == NULL) {
		LERROR("failed to allocate memory for threads\n");
		rc = -ENOMEM;
		pthread_attr_destroy(&attr);
		return rc;
	}

#ifdef HAVE_ZFS
	if (backfs_type == LBT_ZFS) {
		os = global_info->gi_objset;
		dn = DMU_META_DNODE(os);
		max_offset = (dn->dn_maxblkid + 1) * dn->dn_datablksz;
		max_object = max_offset >> DNODE_SHIFT;

		left = max_object % num_threads;
		avg = max_object / num_threads;

		for (i = 0; i < num_threads; i++) {
			start_index[i] = avg * i;
			end_index[i] = avg * (i + 1);
		}
		end_index[num_threads - 1] += left;
	}
#endif

	for (i = 0; i < num_threads; i++) {
		tmp_pinfo = &infos[i];
		tmp_pinfo->ti_instance = instance;
		tmp_pinfo->ti_thread_index = i;
		tmp_pinfo->ti_policy = policy;
		tmp_pinfo->ti_global_info = global_info;
		tmp_pinfo->ti_stopping = false;
		tmp_pinfo->ti_stopped = false;
		tmp_pinfo->ti_started = false;

#ifdef HAVE_ZFS
		if (backfs_type == LBT_ZFS) {
			zor[i] = calloc(1, sizeof(struct zfs_obj_range));
			if (zor[i] == NULL) {
				LERROR("failed to allocate memory for zfs object range\n");
				rc = -ENOMEM;
				break;
			}
			zor[i]->zfs_obj_start = start_index[i];
			zor[i]->zfs_obj_end = end_index[i];
			zor[i]->zfs_obj_max = max_object;
			zor[i]->zfs_obj_count = 0;
			tmp_pinfo->private = zor[i];
			zor[i] = NULL;
		}
#endif
#if 0
		rc = counter_list_init(policy, &tmp_pinfo->ti_counter_list,
				       workspace, i);
		if (rc) {
			LERROR("failed to init counter list\n");
			break;
		}

		rc = classify_list_init(policy, &tmp_pinfo->ti_classify_list,
					workspace, i);
		if (rc) {
			LERROR("failed to init classify list\n");
			counter_list_fini(&tmp_pinfo->ti_counter_list);
			break;
		}
#endif

		switch (backfs_type) {
		case LBT_LDISKFS:
			rc = pthread_create(&infos[i].ti_thread_id, &attr,
					    &ldiskfs_scan_thread, &infos[i]);
			if (rc == 0)
				tmp_pinfo->ti_started = true;
			break;
#ifdef HAVE_ZFS
		case LBT_ZFS:
			tid = lipe_zthread_create((void *)(infos + i), &attr,
						  num_threads - i - 1,
						(thread_func_t)zfs_scan_thread);
			if (tid < 0)
				rc = tid;
			else {
				tmp_pinfo->ti_thread_id = tid;
				tmp_pinfo->ti_started = true;

			}
			break;
#endif
		default:
			rc = -EINVAL;
			LERROR("invalid backend filesystem type\n");
			break;
		}

		if (rc) {
			LERROR("failed to create thread\n");
			//classify_list_fini(&tmp_pinfo->ti_classify_list);
			//counter_list_fini(&tmp_pinfo->ti_counter_list);
			break;
		}
	}

	/* destroy the thread attribute object, since it is no longer needed */
	ret = pthread_attr_destroy(&attr);
	if (ret) {
		LERROR("failed to destroy thread attribute\n");
		if (rc == 0)
			rc = ret;
	}

	if (rc) {
		scan_threads_abort(infos, i);
		return rc;
	}

	*pinfo = infos;
	return 0;
}

int scan_threads_join(struct thread_info *infos, int num_threads,
		      struct scan_result *result,
		      struct counter_list *sum_counter_list,
		      struct classify_list *sum_classify_list,
		      enum lipe_backfs_type backfs_type)
{
	int			 rc;
	int			 ret = 0;
	int			 i;
	struct scan_result	*t_result;
	struct thread_info	*pinfo;
	int			 active_number = num_threads;

	while (active_number != 0) {
		sleep(1);
		active_number = 0;
		for (i = 0; i < num_threads; i++) {
			pinfo = &infos[i];
			if (!pinfo->ti_stopped)
				active_number++;
		}
		LINFO("Scanning with [%d] thread(s)\n", active_number);
	}

	for (i = 0; i < num_threads; i++) {
		pinfo = &infos[i];
		rc = pthread_join(pinfo->ti_thread_id, NULL);
		if (rc) {
			LERROR("failed to join thread\n");
			if (ret == 0)
				ret = rc;
			continue;
		}

		t_result = &pinfo->ti_result;

		switch (backfs_type) {
		case LBT_LDISKFS:
			LDEBUG("thread [%d] scanning inodes in %d.%06u seconds, %llu groups, %llu inodes total, matched %llu files, matched %llu dirs\n",
			       i,
			       (int) t_result->sr_time_diff.tv_sec,
			       (unsigned int) t_result->sr_time_diff.tv_usec,
			       t_result->sr_number_groups,
			       t_result->sr_number_inodes,
			       t_result->sr_number_files,
			       t_result->sr_number_dirs);

			result->sr_number_groups += t_result->sr_number_groups;
			break;
#ifdef HAVE_ZFS
		case LBT_ZFS:
			LDEBUG("thread [%d] scanning inodes in %d.%06u seconds, %llu inodes total, matched %llu files, matched %llu dirs\n",
			       i,
			       (int) t_result->sr_time_diff.tv_sec,
			       (unsigned int) t_result->sr_time_diff.tv_usec,
			       t_result->sr_number_inodes,
			       t_result->sr_number_files,
			       t_result->sr_number_dirs);
			break;
#endif
		default:
			ret = -EINVAL;
			LERROR("invalid backend filesystem type\n");
			break;
		}

		result->sr_number_inodes += t_result->sr_number_inodes;
		result->sr_number_files += t_result->sr_number_files;
		result->sr_number_dirs += t_result->sr_number_dirs;

#if 0
		counter_list_sum(sum_counter_list, &pinfo->ti_counter_list);
		counter_list_fini(&pinfo->ti_counter_list);
		rc = classify_list_sum(sum_classify_list,
				       &pinfo->ti_classify_list);
		if (rc) {
			LERROR("failed to sum classify list\n");
			if (ret == 0)
				ret = rc;
		}
		classify_list_fini(&pinfo->ti_classify_list);
#endif
	}

	for (i = 0; i < num_threads; i++) {
		pinfo = &infos[i];
		if (pinfo->private != NULL) {
			free(pinfo->private);
			pinfo->private = NULL;
		}
	}
	free(infos);

	return ret;
}

void lipe_policy_attrs_reset(struct lipe_policy_attrs *attrs)
{
	struct lov_user_md *lum = attrs->lpa_lum;
	int lum_size = attrs->lpa_lum_size;

	memset(lum, 0, lum_size);
	memset(attrs, 0, sizeof(*attrs));
	attrs->lpa_lum = lum;
	attrs->lpa_lum_size = lum_size;
}

int lipe_policy_attrs_init(struct lipe_policy_attrs *attrs)
{
	attrs->lpa_lum_size = lov_user_md_size(LOV_MAX_STRIPE_COUNT,
					       LOV_USER_MAGIC_V3);
	attrs->lpa_lum = malloc(attrs->lpa_lum_size);
	if (attrs->lpa_lum == NULL)
		return -ENOMEM;
	return 0;
}

void lipe_policy_attrs_fini(struct lipe_policy_attrs *attrs)
{
	free(attrs->lpa_lum);
}

int decode_linkea(__u64 object, char *buf, ssize_t size)
{
	struct link_ea_header	*leh;
	struct link_ea_entry	*lee;
	int			 i;
	__u64			 length;
	int			 reclen;
	struct lu_fid		 pfid;

	leh = (struct link_ea_header *)buf;
	if (leh->leh_magic == __swab32(LINK_EA_MAGIC)) {
		leh->leh_magic = LINK_EA_MAGIC;
		leh->leh_reccount = __swab32(leh->leh_reccount);
		leh->leh_len = __swab64(leh->leh_len);
	}
	if (leh->leh_magic != LINK_EA_MAGIC) {
		fprintf(stderr,
			"%llu: magic mismatch, expected 0x%lx, got 0x%x\n",
			object, LINK_EA_MAGIC, leh->leh_magic);
		return -1;
	}
	if (leh->leh_reccount == 0) {
		fprintf(stderr, "%llu: empty record count\n", object);
		return -1;
	}
	if (leh->leh_len > size) {
		fprintf(stderr,
			"%llu: invalid length %llu, should smaller than %zd\n",
			object, leh->leh_len, size);
		return -1;
	}

	length = sizeof(struct link_ea_header);
	lee = (struct link_ea_entry *)(leh + 1);
	LDEBUG("%llu: count %u\n", object, leh->leh_reccount);
	for (i = 0; i < leh->leh_reccount; i++) {
		reclen = (lee->lee_reclen[0] << 8) | lee->lee_reclen[1];
		length += reclen;
		if (length > leh->leh_len) {
			fprintf(stderr,
				"%llu: length exceeded, expected %lld, got %lld\n",
				object, leh->leh_len, length);
			return -1;
		}
		fid_be_to_cpu(&pfid, (struct lu_fid *)&lee->lee_parent_fid);
		memcpy(&lee->lee_parent_fid, &pfid, sizeof(pfid));

		LDEBUG("    %d: pfid "DFID", name '%s'\n", i,
			     PFID(&pfid), lee->lee_name);
		lee = (struct link_ea_entry *)((char *)lee + reclen);
	}

	if (length != leh->leh_len) {
		fprintf(stderr,
			"%llu: length mismatch, expected %lld, got %lld\n",
			object, leh->leh_len, length);
		return -1;
	}

	return 0;
}

#ifdef HAVE_LAYOUT_BY_XATTR
int decode_lum(struct lov_user_md *lum, ssize_t size)
{
	struct llapi_layout *layout;

	layout = llapi_layout_get_by_xattr(lum, size, 0);
	if (layout == NULL)
		return -errno;
	llapi_layout_free(layout);
	return 0;
}
#else /* !HAVE_LAYOUT_BY_XATTR */
int decode_lum(struct lov_user_md *lum, ssize_t size)
{
#if __BYTE_ORDER == __BIG_ENDIAN
	struct lov_user_md_v3		*lumv3 = (void *)lum;
	struct lov_user_ost_data_v1	*objs;
	int				 i;

	__swab32s(&lum->lmm_magic);
	__swab32s(&lum->lmm_pattern);
	__swab64s(&lum->lmm_oi.oi.oi_id);
	__swab32s(&lum->lmm_stripe_size);
	__swab16s(&lum->lmm_stripe_count);
	__swab16s(&lum->lmm_layout_gen);
#endif
	if (lum->lmm_magic != LOV_USER_MAGIC_V1 &&
	    lum->lmm_magic != LOV_USER_MAGIC_V3) {
		LERROR("LOV EA magic %u is unsupported\n", lum->lmm_magic);
		return -1;
	}

	if ((lum->lmm_pattern & ~LOV_PATTERN_F_MASK) != LOV_PATTERN_RAID0) {
		LERROR("Unsupported LOV EA pattern %u\n",
		       lum->lmm_pattern);
		return -EOPNOTSUPP;
	}

#if __BYTE_ORDER == __BIG_ENDIAN
	if (lum->lmm_magic == LOV_USER_MAGIC_V1) {
		objs = &lum->lmm_objects[0];
	} else {
		LASSERT(lum->lmm_magic == LOV_USER_MAGIC_V3);
		objs = &lumv3->lmm_objects[0];
	}

	for (i = 0; i < lum->lmm_stripe_count; i++, objs++) {
		if (fid_seq_is_mdt0(objs->l_ost_oi.oi.oi_seq)) {
			__swab64s(&objs->l_ost_oi.oi.oi_id);
			__swab64s(&objs->l_ost_oi.oi.oi_seq);
		} else {
			__swab64s(&objs->l_ost_oi.oi_fid.f_seq);
			__swab32s(&objs->l_ost_oi.oi_fid.f_oid);
			__swab32s(&objs->l_ost_oi.oi_fid.f_ver);
		}
	}
#endif
	return 0;
}
#endif /* !HAVE_LAYOUT_BY_XATTR */

/**
 * Swab, if needed, HSM structure which is stored on-disk in little-endian
 * order.
 *
 * \param attrs - is a pointer to the HSM structure to be swabbed.
 */
void lustre_hsm_swab(struct hsm_attrs *attrs)
{
#if __BYTE_ORDER == __BIG_ENDIAN
	__swab32s(&attrs->hsm_compat);
	__swab32s(&attrs->hsm_flags);
	__swab64s(&attrs->hsm_arch_id);
	__swab64s(&attrs->hsm_arch_ver);
#endif
};


/*
 * Swab and extract HSM attributes from on-disk xattr.
 */
int lustre_hsm2user(struct hsm_attrs *disk,
		    struct hsm_user_state *hus)
{
	/* unpack HSM attributes */
	lustre_hsm_swab(disk);

	/* fill hsm_user_state structure */
	hus->hus_states = disk->hsm_flags;
	hus->hus_archive_id  = disk->hsm_arch_id;

	return 0;
}
