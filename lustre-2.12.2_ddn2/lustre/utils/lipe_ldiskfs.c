/*
 * Copyright (c) 2017, DDN Storage Corporation.
 */
/*
 *
 * Tool for scanning the MDT and print list of matched files.
 *
 * Author: Li Xi <lixi@ddn.com>
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <config.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <ext2fs/ext2fs.h>
#include <getopt.h>
#include <sys/time.h>
#include <assert.h>
#include "mount_utils.h"

#ifdef NEW_USER_HEADER
#include <linux/lustre/lustre_user.h>
#else
#include <lustre/lustre_user.h>
#endif

#include <lustre/lustreapi.h>
#include <lustre/include/uapi/linux/lustre/lustre_idl.h>
#include <pthread.h>
#include "lipe_policy.h"
//#include "ldiskfs_read_ldd.h"
//#include "fake_lustre_idl.h"
//#include "debug.h"
//#include "lipe_config.h"
//#include "lustre_ea.h"
//#include "cmd.h"
//#include "lustre_ea_ldiskfs.h"

#ifndef EXT2_ATTR_INDEX_TRUSTED
#define EXT2_ATTR_INDEX_TRUSTED                4
#endif
#ifndef EXT2_ET_EA_NAME_NOT_FOUND
#define EXT2_ET_EA_NAME_NOT_FOUND                (2133571512L)
#endif

#if 0
/*
 * XXX: this is made to let lpurge work with both versions of e2fsprogs:
 * 	old slow API and new fast... but we want to get rid of this
 * 	as quickly as possible
 */
static errcode_t __ext2fs_attr_get(ext2_filsys fs, ext2_ino_t ino,
				   int name_index, const char *name,
				   char *buffer, size_t buffer_size,
				   int *easize)
{
	void *p;
	size_t size = 0;
	struct ext2_xattr_handle *handle;
	errcode_t retval;
	char eaname[128];
	char *prefix;

	if (name_index == EXT2_ATTR_INDEX_TRUSTED)
		prefix="trusted.";
	snprintf(eaname, sizeof(eaname), "%s%s", prefix, name);

	retval = ext2fs_xattrs_open(fs, ino, &handle);
	if (retval)
		return retval;

	retval = ext2fs_xattrs_read(handle);
	if (retval)
		goto err;

	retval = ext2fs_xattr_get(handle, eaname, &p, &size);
	if (retval)
		goto err;
	if (size <= buffer_size) {
		memcpy(buffer, p, size);
		*easize = size;
	} else
		retval = -EOVERFLOW;

	ext2fs_free_mem(&p);
err:
	(void) ext2fs_xattrs_close(&handle);
	return retval;
}
#endif


int get_link_ea_ldiskfs(struct lipe_object *object,
		       struct lipe_policy_attrs *attrs)
{
	int			 size = 0;
	int			 rc;
	char			*buf;
	ext2_filsys		 fs = object->u.lo_ldiskfs.lol_fs;
	ext2_ino_t		 ino = object->u.lo_ldiskfs.lol_ino;
	struct ext2_inode	*inode = object->u.lo_ldiskfs.lol_inode;

	buf = attrs->lpa_leh_buf;
	/*
	 * Need to clear the buffer, otherwise lee->lee_name will keep dirty
	 * data of last inode
	 */
	memset(buf, 0, MAX_LINKEA_SIZE);
	rc = ext2fs_attr_get(fs, inode, EXT2_ATTR_INDEX_TRUSTED,
			     XATTR_NAME_LINK + strlen("trusted."),
			     buf, MAX_LINKEA_SIZE, &size);
	if (rc == EXT2_ET_EA_NAME_NOT_FOUND) {
		LDEBUG("inode [%d] has no [%s] xattr ignoring\n",
		       ino, XATTR_NAME_LINK);
		return -1;
	} else if (rc) {
		LERROR("failed to get [%s] xattr for inode [%d], rc = %d\n",
		       XATTR_NAME_LINK, ino, rc);
		return -1;
	}

	rc = decode_linkea(object->lo_id, buf, size);
	if (rc) {
		LERROR("failed to decode linkea for inode [%d]\n", ino);
		return -1;
	}
	attrs->lpa_attr_bits |= LIPE_POLICY_BIT_LINKEA;
	return 0;
}

int get_lma_ea_ldiskfs(struct lipe_object *object,
		    struct lipe_policy_attrs *attrs)
{
	char			 buf[MAX_LINKEA_SIZE];
	struct lustre_mdt_attrs	*lma;
	int			 size;
	int			 rc;
	ext2_filsys		 fs = object->u.lo_ldiskfs.lol_fs;
	struct ext2_inode	*inode = object->u.lo_ldiskfs.lol_inode;

	rc = ext2fs_attr_get(fs, inode, EXT2_ATTR_INDEX_TRUSTED,
			     XATTR_NAME_LMA + strlen("trusted."),
			     buf, MAX_LINKEA_SIZE, &size);
	if (rc == EXT2_ET_EA_NAME_NOT_FOUND) {
		LDEBUG("inode [%d] has no [%s] xattr ignoring\n",
		       ino, XATTR_NAME_LMA);
		return -1;
	} else if (rc) {
		/*LERROR("failed to get [%s] xattr for inode [%d], rc = %d\n",
		       XATTR_NAME_LMA, ino, rc);*/
		return -1;
	}

	lma = (struct lustre_mdt_attrs *)buf;
	fid_le_to_cpu(&attrs->lpa_fid, &lma->lma_self_fid);
	attrs->lpa_attr_bits |= LIPE_POLICY_BIT_LMAEA;
	return 0;
}

int get_fid_ea_ldiskfs(struct lipe_object *object,
		    struct lipe_policy_attrs *attrs)
{
	ext2_filsys		 fs = object->u.lo_ldiskfs.lol_fs;
	struct ext2_inode	*inode = object->u.lo_ldiskfs.lol_inode;
	struct filter_fid	*ff = &attrs->lpa_ff;
	struct lu_fid		 pfid;
	int			 size;
	int			 rc;

	rc = ext2fs_attr_get(fs, inode, EXT2_ATTR_INDEX_TRUSTED,
			     XATTR_NAME_FID + strlen("trusted."),
			     (char *)ff, sizeof(*ff), &size);
	if (rc == EXT2_ET_EA_NAME_NOT_FOUND) {
		LDEBUG("inode [%d] has no [%s] xattr ignoring\n",
		       ino, XATTR_NAME_FID);
		return -1;
	} else if (rc) {
		/*LERROR("failed to get [%s] xattr for inode [%d], rc = %d\n",
		       XATTR_NAME_FID, ino, rc);*/
		return -1;
	}

	fid_le_to_cpu(&pfid, &ff->ff_parent);
	ff->ff_parent = pfid;
	attrs->lpa_attr_bits |= LIPE_POLICY_BIT_FID;
	attrs->lpa_ff_size = size;
	return 0;
}

int get_hsm_ea_ldiskfs(struct lipe_object *object,
		       struct lipe_policy_attrs *attrs,
		       bool watch)
{
	char			 buf[MAX_LINKEA_SIZE];
	struct hsm_attrs	*hsm;
	int			 rc;
	int			 size;
	struct hsm_user_state	*hus = &attrs->lpa_hsm_state;
	ext2_filsys		 fs = object->u.lo_ldiskfs.lol_fs;
	ext2_ino_t		 ino = object->u.lo_ldiskfs.lol_ino;
	struct ext2_inode	*inode = object->u.lo_ldiskfs.lol_inode;

	LASSERT(sizeof(*hsm) < MAX_LINKEA_SIZE);

	rc = ext2fs_attr_get(fs, inode, EXT2_ATTR_INDEX_TRUSTED,
			     XATTR_NAME_HSM + strlen("trusted."),
			     buf, MAX_LINKEA_SIZE, &size);
	if (rc == EXT2_ET_EA_NAME_NOT_FOUND) {
		hus->hus_states = 0;
		hus->hus_archive_id  = 0;
	} else if (rc) {
		LERROR("failed to get [%s] xattr for inode [%d], rc = %d\n",
		       XATTR_NAME_HSM, ino, rc);
		return -1;
	} else {
		hsm = (struct hsm_attrs *)buf;
		rc = lustre_hsm2user(hsm, hus);
		if (rc) {
			LERROR("failed to extract [%s] xattr for inode [%d], rc = %d\n",
			       XATTR_NAME_HSM, ino, rc);
			return rc;
		}
	}
	attrs->lpa_attr_bits |= LIPE_POLICY_BIT_HSMEA;
	return 0;
}

int get_lum_ea_ldiskfs(struct lipe_object *object,
		       struct lipe_policy_attrs *attrs,
		       bool watch)
{
	int			 rc;
	int			 size;
	struct lov_user_md	*lov = attrs->lpa_lum;
	ext2_filsys		 fs = object->u.lo_ldiskfs.lol_fs;
	ext2_ino_t		 ino = object->u.lo_ldiskfs.lol_ino;
	struct ext2_inode	*inode = object->u.lo_ldiskfs.lol_inode;

	rc = ext2fs_attr_get(fs, inode, EXT2_ATTR_INDEX_TRUSTED,
			     XATTR_NAME_LOV + strlen("trusted."),
			     (char *)lov, attrs->lpa_lum_size, &size);
	if (rc == EXT2_ET_EA_NAME_NOT_FOUND) {
		return -ENODATA;
	} else if (rc) {
		LERROR("failed to get [%s] xattr for inode [%d], rc = %d\n",
		       XATTR_NAME_LOV, ino, rc);
		return -ENODATA;
	}

	rc = decode_lum(lov, size);
	if (rc) {
		LERROR("failed to decode lovea for inode [%d]\n", ino);
		return rc;
	}
	attrs->lpa_attr_bits |= LIPE_POLICY_BIT_LOVEA;
	return 0;
}

#ifdef HAVE_LAZY_SIZE_ON_MDT
/*
 * llapi_layout_get_by_xattr() and LSoM are both included in Lustre-2.12,
 * so no need to duplicate the macros.
 */
int get_som_ea_ldiskfs(struct lipe_object *object,
		       struct lipe_policy_attrs *attrs,
		       bool watch)
{
	int			 rc;
	int			 size;
	struct lustre_som_attrs	*som = &attrs->lpa_som;
	ext2_filsys		 fs = object->u.lo_ldiskfs.lol_fs;
	ext2_ino_t		 ino = object->u.lo_ldiskfs.lol_ino;
	struct ext2_inode	*inode = object->u.lo_ldiskfs.lol_inode;

	rc = ext2fs_attr_get(fs, inode, EXT2_ATTR_INDEX_TRUSTED,
			     XATTR_NAME_SOM + strlen("trusted."),
			     (char *)som, sizeof(*som), &size);
	if (rc == EXT2_ET_EA_NAME_NOT_FOUND) {
		LDEBUGW(watch, "inode [%d] has no [%s] xattr\n",
			ino, XATTR_NAME_SOM);
		return -ENODATA;
	} else if (rc) {
		LERROR("failed to get [%s] xattr for inode [%d], rc = %d\n",
		       XATTR_NAME_SOM, ino, rc);
		return -ENODATA;
	}

	if (size != sizeof(*som)) {
		LERROR("unexpected size of [%s] xattr for inode [%s], expected [%d], got [%d]\n",
		       XATTR_NAME_SOM, ino, sizeof(*som), size);
		return -ENODATA;
	}

	lustre_som_swab(som);
	attrs->lpa_attr_bits |= LIPE_POLICY_BIT_SOM;
	return 0;
}
#endif /* HAVE_LAZY_SIZE_ON_MDT */

struct lipe_backfs_operations ldiskfs_operations = {
	.get_fid_ea = get_fid_ea_ldiskfs,
	.get_lma_ea = get_lma_ea_ldiskfs,
	.get_link_ea = get_link_ea_ldiskfs,
	.get_hsm_ea = get_hsm_ea_ldiskfs,
	.get_lum_ea = get_lum_ea_ldiskfs,
#ifdef HAVE_LAZY_SIZE_ON_MDT
	.get_som_ea = get_som_ea_ldiskfs,
#endif
};
/*
 * done_group callback for inode scan.
 */
static errcode_t done_group_callback(ext2_filsys fs, ext2_inode_scan scan,
				     dgrp_t group, void *vp)
{
	struct scan_result *result = (struct scan_result *)vp;

	result->sr_number_groups++;
	result->sr_group_start++;
	if (result->sr_group_start >= result->sr_group_end)
		return 1;
	return 0;
}

static void ldiskfs_copy_inode_attrs(struct lipe_policy_attrs *attrs,
				     struct ext2_inode *inode)
{
	attrs->lpa_atime_ms = ((int64_t)inode->i_atime) * 1000;
	attrs->lpa_mtime_ms = ((int64_t)inode->i_mtime) * 1000;
	attrs->lpa_ctime_ms = ((int64_t)inode->i_ctime) * 1000;
	attrs->lpa_size = (int64_t)inode->i_size;
	attrs->lpa_mode = (int64_t)inode->i_mode;
	attrs->lpa_uid = (int64_t)inode->i_uid;
	attrs->lpa_gid = (int64_t)inode->i_gid;
	attrs->lpa_flags = (int64_t)inode->i_flags;
	attrs->lpa_nlinks = (int64_t)inode->i_links_count;
	attrs->lpa_blocks = (int32_t)inode->i_blocks; /* XXX: _hi part */
}

static int ldsikfs_scan_get_next_group_batch(struct global_info *ginfo,
					     unsigned long *next_start,
					     unsigned long *next_end)
{
	int ret = 0;

	assert(ginfo->gi_group_current <= ginfo->gi_group_end);

	pthread_mutex_lock(&ginfo->gi_mutex);

	if (ginfo->gi_group_current == ginfo->gi_group_end) {
		ret = 1;
		goto out;
	}
	*next_start = ginfo->gi_group_current;
	ginfo->gi_group_current += ginfo->gi_group_batch;
	if (ginfo->gi_group_current >= ginfo->gi_group_end)
		ginfo->gi_group_current = ginfo->gi_group_end;
	*next_end = ginfo->gi_group_current;
out:
	pthread_mutex_unlock(&ginfo->gi_mutex);

	return ret;
}

static int ldiskfs_scan_groups(ext2_inode_scan scan,
			       ext2_filsys fs,
			       struct lipe_policy_attrs *attrs,
			       struct lipe_policy_sysattrs *sysattrs,
			       struct lipe_object *object,
			       struct ext2_inode *inode, int inode_size,
			       struct thread_info *info,
			       unsigned long start_group)
{
	const char *srv_name;
	struct lipe_policy *policy;
	struct lipe_instance *instance;
	struct global_info   *ginfo;
	struct counter_list *counter_list = NULL;
	struct classify_list *classify_list = NULL;
	struct scan_result *result;
	struct lu_fid *watch_fid;
	bool lustre;
	bool abort_failure;
	bool all_inode;
	int rc;

	srv_name = info->ti_ldd->ldd_svname;
	instance = info->ti_instance;
	ginfo = info->ti_global_info;
	policy = info->ti_policy;
	//counter_list = &info->ti_counter_list;
	//classify_list = &info->ti_classify_list;
	result = &info->ti_result;

	watch_fid = ginfo->gi_watch_fid;
	lustre = ginfo->gi_lustre;
	abort_failure = ginfo->gi_abort_failure;
	all_inode = ginfo->gi_all_inode;

	rc = ext2fs_inode_scan_goto_blockgroup(scan, start_group);
	if (rc) {
		LERROR("failed to goto block group [%lu]\n",
		       start_group);
		return rc;
	}

	while (!ext2fs_get_next_inode_full(scan, &object->u.lo_ldiskfs.lol_ino,
					   inode, inode_size)) {
		ext2_ino_t ino;

		ino = object->u.lo_ldiskfs.lol_ino;
		if (ino == 0)
			break;

		object->lo_id = (__u64)ino;

		if (info->ti_stopping) {
			LERROR("thread abort without finishing\n");
			break;
		}

		if (ext2fs_fast_test_inode_bitmap2(fs->inode_map, ino) == 0)
			/* deleted - always skip for now */
			continue;

		if (!LINUX_S_ISDIR(inode->i_mode) &&
		    (inode->i_flags & EXT2_NODUMP_FL)) {
			/* skip files which are not to be backuped */
			ext2fs_fast_unmark_inode_bitmap2(fs->inode_map, ino);
			continue;
		}

		result->sr_number_inodes++;
		lipe_policy_attrs_reset(attrs);
		ldiskfs_copy_inode_attrs(attrs, inode);
		attrs->lpa_attr_bits = LIPE_POLICY_BIT_ATTR;

		rc = lipe_policy_apply(object, policy, instance, attrs,
				       sysattrs, result, srv_name,
				       abort_failure, counter_list,
				       classify_list, watch_fid, all_inode,
				       lustre);
		if (rc)  {
			LDEBUG("failed to apply policy on inode [%d]\n", ino);
			rc = 0;
		}
	}

	return rc;
}

void *ldiskfs_scan_thread(void *arg)
{
	struct thread_info	     *info = arg;
	struct lipe_instance	     *instance = info->ti_instance;
	const char		     *dev = instance->li_device;
	//struct counter_list	     *counter_list = &info->ti_counter_list;
	//struct classify_list	     *classify_list = &info->ti_classify_list;
	struct scan_result	     *result = &info->ti_result;
	struct lipe_object	     object;
	ext2_filsys		     fs;
	ext2_inode_scan		     scan;
	struct ext2_inode	     *inode;
	struct lipe_policy_attrs     attrs;
	struct lipe_policy_sysattrs  sysattrs;
	struct global_info	     *ginfo = info->ti_global_info;
	int			     inode_size;
	int			     rc;

	rc = lipe_policy_attrs_init(&attrs);
	if (rc != 0) {
		LERROR("failed to init atttrs: not enough memory\n");
		goto out;
	}

	object.lo_backfs_ops = &ldiskfs_operations;
	object.lo_backfs_type = LBT_LDISKFS;

	//classify_list = &info->ti_classify_list;

	result->sr_number_dirs = 0;
	result->sr_number_files = 0;
	result->sr_number_inodes = 0;
	result->sr_number_groups = 0;

	gettimeofday(&result->sr_time_start, NULL);
	sysattrs.lps_sys_time = result->sr_time_start.tv_sec * 1000 +
		result->sr_time_start.tv_usec / 1000;
	sysattrs.lps_attr_bits = LIPE_POLICY_BIT_SYSATTR_TIME;

	rc = ext2fs_open(dev, EXT2_FLAG_SOFTSUPP_FEATURES,
			 0, 0, unix_io_manager, &fs);
	if (rc != 0) {
		LERROR("failed to ext2fs_open [%s]: %s\n",
		       dev, error_message(rc));
		goto out_free_attrs;
	}

	object.u.lo_ldiskfs.lol_fs = fs;

	rc = ext2fs_read_inode_bitmap(fs);
	if (rc) {
		LERROR("failed to ext2fs_read_inode_bitmap on [%s]: %s\n", dev,
		       error_message(rc));
		goto out_close;
	}

	rc = ext2fs_open_inode_scan(fs, fs->inode_blocks_per_group, &scan);
	if (rc) {
		LERROR("failed to ext2fs_open_inode_scan on [%s]: %s\n", dev,
		       error_message(rc));
		goto out_close;
	}


	inode_size = EXT2_INODE_SIZE(fs->super);
	inode = malloc(inode_size);
	if (!inode) {
		rc = -1;
		LERROR("failed to allocte inode buffer\n");
		goto out_scan;
	}

	object.u.lo_ldiskfs.lol_inode = inode;

	while (1) {
		unsigned long start_group;
		unsigned long end_group;

		rc = ldsikfs_scan_get_next_group_batch(ginfo,
						       &start_group,
						       &end_group);
		if (rc)
			break;

		result->sr_group_start = start_group;
		result->sr_group_end = end_group;

		LDEBUG("scanning groups [%lu, %lu)\n",
		       result->sr_group_start, result->sr_group_end);

		ext2fs_set_inode_callback(scan, done_group_callback, result);

		rc = ldiskfs_scan_groups(scan, fs, &attrs, &sysattrs,
					 &object, inode, inode_size,
					 info, start_group);
		if (rc)
			goto out_free;

		result->sr_group_count += (end_group - start_group);
		LDEBUG("scaned groups [%lu, %lu)\n", start_group, end_group);
	}
	//counter_list_flush(counter_list);
	//classify_list_flush(classify_list);

out_free:
	free(inode);
out_scan:
	ext2fs_close_inode_scan(scan);
out_close:
	ext2fs_close(fs);
out_free_attrs:
	lipe_policy_attrs_fini(&attrs);
out:
	gettimeofday(&result->sr_time_end, NULL);
	diff_timevals(&result->sr_time_start, &result->sr_time_end,
		      &result->sr_time_diff);
	info->ti_stopped = true;
	return NULL;
}

#define DEFAULT_GROUP_BATCH 10

int ldiskfs_scan(struct lipe_instance *instance,
		 struct lipe_policy *policy,
		 struct scan_result *result,
		 struct counter_list *sum_counter_list,
		 struct classify_list *sum_classify_list,
		 int num_threads,
		 const char *workspace,
		 bool abort_failure)
{
	const char		*dev = instance->li_device;
	ext2_filsys		 fs;
	int			 rc;
	unsigned long		 group_total, group_batch;
	struct thread_info	*infos = NULL;
	struct global_info	 global_info;

	global_info.gi_workspace = workspace;
	global_info.gi_abort_failure = abort_failure;
	pthread_mutex_init(&global_info.gi_mutex, NULL);

	rc = ext2fs_open(dev, EXT2_FLAG_SOFTSUPP_FEATURES,
			 0, 0, unix_io_manager, &fs);
	if (rc != 0) {
		LERROR("failed to ext2fs_open [%s]: %s\n",
		       dev, error_message(rc));
		return -1;
	}

	group_total = fs->group_desc_count;
	global_info.gi_group_end = group_total;
	global_info.gi_group_current = 0;

	/* keep the scan batch in a reasonable small one */
	group_batch = group_total / num_threads;
	if (!group_batch)
		group_batch = 1;
	else if (group_batch > DEFAULT_GROUP_BATCH)
		group_batch = DEFAULT_GROUP_BATCH;

	global_info.gi_group_batch = group_batch;

	rc = scan_threads_start(&infos, num_threads, instance, policy,
				&global_info, LBT_LDISKFS);
	if (rc) {
		LERROR("failed to start threads\n");
		goto out_close;
	}

	scan_threads_join(infos, num_threads, result, sum_counter_list,
			  sum_classify_list, LBT_LDISKFS);

out_close:
	ext2fs_close(fs);
	if (rc != 0)
		return rc;
	return 0;
}
