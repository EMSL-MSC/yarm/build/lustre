/*
 * Copyright (c) 2019, DDN Storage Corporation.
 *
 * Author: Alexey Zhuravlev <bzzz@whamcloud.com>
 */

/*
 * How to use:
 *  - mount Lustre clients on all MDS servers
 *  - make a configuration (see example in usage())
 *  - run!
 */

/*
 * Very Rough Design
 * The utility monitors available space. Once available space goes
 * below "low" threshold it scans the device putting found objects
 * into "baskets" where each basket is dedicated for specific range
 * of "modify time". Then the utility tries to get rid of found
 * objects starting from the oldest basket. The utility consider all
 * objects as replicas (optimistically) and calls lfs split -d against
 * them. If it's lucky, then given object is removed release space.
 * This process repeats until all found objects are processed or until
 * "high" threshold of available space is reached.
 */

/*
 * TODO
 *  - convert blocks to 1K units
 *  - be able to remove replica on specific OSTs
 *    so that it works w/o pool specified in striping info
 *    expire all data after some period
 *    should help if partial result gave enough space and
 *    new low-hit took very log
 *  - limit number of objects to save during scanning
 *    this introduce requirement to resume scanning from specific point
 *    (multiple points when mutlithreading is used), otherwise we can
 *    get stuck finding the same objects again and again
 *  - multiple threads (locking)
 *  - handle failed ssh
 *  - slow scanning before low is hit
 *  - do not use Lustre client to map fid to mdt#
 *  - use inbound protocol to remove replica
 *    do not depend on SSH, manual mdt->host mapping
 *  - persistent hint on OST objects with replica
 *    to skip non-replicated files
 *  - take OST load into account
 *  - OST size can change runtime
 *  - ZFS support
 *
 */

#include <stdlib.h>
#include <errno.h>
#include <getopt.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <poll.h>
#include <glob.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <time.h>
#include <linux/unistd.h>
#include <linux/kernel.h>
#include <sys/wait.h>
#include <sys/sysinfo.h>
#include <linux/lustre/lustre_user.h>
#include <lustre/lustreapi.h>
#include <linux/lustre/lustre_idl.h>
#include <linux/lustre/lustre_fid.h>
#include <linux/lustre/lustre_cfg.h>
#include <libcfs/util/hash.h>
#include <libcfs/util/list.h>
#include <libcfs/util/parser.h>
#include <libcfs/util/param.h>
#include "lipe_policy.h"

struct lpurge_object_pack {
	struct list_head lop_list;
	unsigned lop_max;
	unsigned lop_nr;
	int	 lop_pid;
	struct lu_fid lop_fids[];
};

struct lpurge_slot {
	unsigned long ls_age;
	unsigned long ls_found;
	unsigned long ls_space;
	unsigned long ls_stored; /* FIDs stored */
	unsigned ls_scan;
	struct list_head ls_obj_list;

};

struct mds {
	char *host;
	char *mnt;
};

#define MAX_MDTS	128
struct mds mds[MAX_MDTS];
int mdsnr = 0;
char *src_pool = NULL;

#define LPURGE_HIST_MAX	16

struct lpurge_slot lpurge_hist[LPURGE_HIST_MAX];

struct lipe_instance instance;
unsigned long long freelo = 0, freehi = 0;
char *mntpt;
char *ostname = NULL;
char *ostprefix;
unsigned long kbfree = 0;
unsigned long scan_start;
int lustre_fd = -1;
int max_jobs = 2;
int check_interval = 8;
unsigned long oldest = 0;
unsigned long scan_started;

FILE *dfile = NULL;

#define DEBUG(fmt, args...)				\
	do {						\
		if (dfile) {				\
			fprintf(dfile, fmt, ##args);	\
			fflush(dfile);			\
		}					\
	} while (0);

void lpurge_init_result(void)
{
	int age = 3600; /* hour or less */
	int i;

	/*
	 * initialize age slots. the age is exponential in the
	 * first approach, but we can change it to make linear
	 * with some multiplier.
	 */
	for (i = 0; i < LPURGE_HIST_MAX; i++) {
		lpurge_hist[i].ls_age = age;
		lpurge_hist[i].ls_found = 0;
		lpurge_hist[i].ls_space = 0;
		lpurge_hist[i].ls_stored = 0;
		INIT_LIST_HEAD(&lpurge_hist[i].ls_obj_list);
		age = age << 1;
	}
}

void lpurge_reset_result(void)
{
	struct lpurge_object_pack *s, *t;
	int i;

	for (i = 0; i < LPURGE_HIST_MAX; i++) {
		lpurge_hist[i].ls_found = 0;
		lpurge_hist[i].ls_space = 0;
		lpurge_hist[i].ls_stored = 0;
		list_for_each_entry_safe(s, t, &lpurge_hist[i].ls_obj_list, lop_list) {
			list_del(&s->lop_list);
			free(s);
		}
	}
}

void usage(char *prog)
{
	printf("Usage: %s <config file>\n", prog);
	printf("  config example:\n"
	       "\tdevice=lustre-OST0000\n"
	       "\tmount=/mnt/lustre\n"
	       "\tfreelo=20\n"
	       "\tfreehi=30\n"
	       "\tmax_jobs=8\n"
	       "\tpool=<pool>\n"
	       "\tmds=0:mds1host:/mnt/lustre\n"
	       "\tmds=1:mds2host:/mnt/lustre2\n");
	exit(0);
}

static void lpurge_find_device(char *devname)
{
	glob_t paths;
	int rc, i;

	rc = cfs_get_param_paths(&paths, "osd*/%s/fstype", devname);
	if (rc != 0) {
		fprintf(stderr, "can't find device '%s': rc=%d\n",
			devname, errno);
		exit(1);
	}
	for (i = 0; i < paths.gl_pathc; i++) {
		struct stat st;

		if (stat(paths.gl_pathv[i], &st) == -1)
			continue;
		if (!S_ISREG(st.st_mode))
			continue;
		if (ostname) {
			fprintf(stderr, "Multiple OST devices found for '%s'\n",
				devname);
			exit(1);
		}
		ostname = strdup(devname);
		ostprefix = strdup(paths.gl_pathv[i]);
	}
	i = strlen(ostprefix);
	while (i && ostprefix[i] != '/')
		i--;
	ostprefix[i] = 0;

	cfs_free_param_data(&paths);
}

static int lpurge_read_param(const char *param, char *val, const int vallen)
{
	char buf[PATH_MAX];
	int fd, rc;

	snprintf(buf, sizeof(buf), "%s/%s", ostprefix, param);
	fd = open(buf, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "can't open %s: %d\n", buf, errno);
		return -errno;
	}
	rc = read(fd, val, vallen);
	if (rc > 0) {
		while (rc && (val[rc - 1] == '\n' || val[rc - 1] == '\r'))
			rc--;
		val[rc] = 0;
		rc = 0;
	}
	if (rc < 0) {
		fprintf(stderr, "can't read: %d\n", errno);
		return -errno;
	}
	close(fd);
	return rc;
}

static unsigned long lpurge_kbfree(void)
{
	char buf[64];

	lpurge_read_param("kbytesfree", buf, sizeof(buf));
	return atol(buf);
}

static unsigned long lpurge_kbtotal(void)
{
	char buf[64];

	lpurge_read_param("kbytestotal", buf, sizeof(buf));
	return atol(buf);
}

void lpurge_wait_for_low(void)
{
	unsigned long long kbfree;
	while (1) {
		kbfree = lpurge_kbfree();
		DEBUG("free %llu, low %llu\n", kbfree, freelo);
		if (kbfree < freelo)
			break;
		sleep(check_interval);
	}
}

/*
 * so this is a callback for object scanner
 *
 * at first, we ignore non-interesting object
 * for lpurge it's
 *  - all non-regular objects
 *  - all objects w/o FID or parent (MDS) FID
 *  - empty objects
 *
 * make a table indexed by time:
 * hour and yonger - #objects / blocks
 * 64K FIDs - 1MB RAM
 *
 * it's not enough to know object/PFID - we need to know
 * the file has an uptodate replica. is it possible to
 * tell MDS something like "try to release stripes from
 * this OST if you have a good replica" ? preferrable in
 * batches
 */
int lpurge_lipe_callback(struct lipe_instance *instance,
			 struct lipe_object *object,
			struct lipe_policy_attrs *attrs)
{
	struct lpurge_slot *ls = NULL;
	struct lpurge_object_pack *lp = NULL;
	unsigned long age;
	int i;

	/* ignore everything except regular files on OST */
	if ((attrs->lpa_mode & S_IFMT) != S_IFREG)
		return 0;

	if ((attrs->lpa_attr_bits & LIPE_POLICY_BIT_FID) == 0)
		return 0;

	/* to avoid N OSTs to 1 MDT scalability issue we only consider
	 * objects which store 1st stripe */
	if (attrs->lpa_ff.ff_parent.f_ver != 0)
		return 0;

	/* if the object has got ost_layout structure which encodes
	 * whether the object has a mirror, then we can skip objects
	 * with mirror_id=0 (no mirror) */
	if (attrs->lpa_ff_size >= sizeof(struct filter_fid)) {
		if (mirror_id_of(attrs->lpa_ff.ff_layout.ol_comp_id) == 0)
			return 0;
	}

	if (scan_started < attrs->lpa_mtime_ms)
		age = 0;
	else
		age = (scan_started - attrs->lpa_mtime_ms) / 1000;
	if (age > oldest)
		oldest = age;

	/* find slot for given ctime */
	ls = lpurge_hist + LPURGE_HIST_MAX - 1;
	for (i = 0; i < LPURGE_HIST_MAX - 1; i++) {
		if (age < lpurge_hist[i].ls_age) {
			ls = lpurge_hist + i;
			break;
		}
	}
	if (ls->ls_scan == 0) {
		/* this is a partial scan for objects with specific age */
		return 0;
	}

	DEBUG("!found "DFID"/%lld: size %ld block %ld age %ld slot %d\n",
		PFID(&attrs->lpa_ff.ff_parent), object->lo_id,
		(unsigned long)attrs->lpa_size, (unsigned long)attrs->lpa_blocks,
		age, i);

	ls->ls_found++;
	ls->ls_space += attrs->lpa_blocks >> 1;

	/* XXX: limit # of objects stored */
	if (ls->ls_stored > 1024 * 1024)
		return 0;

	if (!list_empty(&ls->ls_obj_list))
		lp = list_entry(ls->ls_obj_list.next, struct lpurge_object_pack, lop_list);
#define PACK_SIZE	(32*1024)
#define PACK_NR ((PACK_SIZE - offsetof(struct lpurge_object_pack, lop_fids[0]))/sizeof(struct lu_fid))
	if (!lp || lp->lop_nr >= lp->lop_max) {
		lp = malloc(PACK_SIZE);
		assert(lp);
		lp->lop_max = PACK_NR;
		lp->lop_nr = 0;
		list_add(&lp->lop_list, &ls->ls_obj_list);
	}
	DEBUG("add "DFID" to %p/%d\n", PFID(&attrs->lpa_ff.ff_parent), lp, lp->lop_nr);
	lp->lop_fids[lp->lop_nr] = attrs->lpa_ff.ff_parent;
	/* a bit hacky - store blocks in fid.f_ver which isn't used at the moment */
	/* XXX: ZFS's blocksize is per-object */
	/* XXX: ldiskfs counts sectors, convert properly */
	lp->lop_fids[lp->lop_nr].f_ver = attrs->lpa_blocks >> 1;
	lp->lop_nr++;
	ls->ls_stored++;

	return 0;
}

#define CMDSIZE		(64 * 1024)

int lpurge_spawn_one(int idx, struct lpurge_object_pack *lp)
{
	char buf[PATH_MAX];
	char **args;
	int pid, k;

	pid = fork();
	if (pid < 0) {
		fprintf(stderr, "can't fork: %d\n", errno);
		exit(1);
	}

	if (pid > 0) {
		DEBUG("spawn %d for %d files on MDT#%d\n",
			pid, lp->lop_nr, idx);
		return pid;
	}

	args = malloc(sizeof(char *) * (lp->lop_nr + 12));
	assert(args);
	args[0] = "/usr/bin/ssh";
	args[1] = mds[idx].host;
	args[2] = "lfs";
	args[3] = "mirror";
	args[4] = "split";
	args[5] = "-d";
	args[6] = "-p";
	args[7] = src_pool;
	for (k = 0; k < lp->lop_nr; k++) {
		snprintf(buf, sizeof(buf), "%s/.lustre/fid/"DFID,
			 mds[idx].mnt, PFID(&lp->lop_fids[k]));
		args[8 + k] = strdup(buf);
	}
	args[8 + k] = NULL;

	close(0);
	close(1);
	close(2);

	/* use debug file for ssh/lfs stderr if defined */
	if (dfile) {
		dup2(fileno(dfile), 1);
		dup2(fileno(dfile), 2);
	} else {
		int fd;
		fd = open("/dev/null", O_WRONLY);
		if (fd >= 0) {
			dup2(fd, 1);
			dup2(fd, 2);
		}
	}

	pid = execve("/usr/bin/ssh", &args[0], environ);
	if (pid < 0) {
		fprintf(stderr, "can't exec: %d\n", pid);
		exit(1);
	}

	return 0;
}

int lpurge_spawn(struct list_head *pm_list)
{
	int jobs[MAX_MDTS] = { 0 };
	struct list_head list; /* list of jobs in execution */
	struct lpurge_object_pack *lp, *tmp;
	int total = 0;
	int i, repeat, pid, status;

	INIT_LIST_HEAD(&list);

next:
	repeat = 0;
	for (i = 0; i < MAX_MDTS; i++) {
		while (!list_empty(&pm_list[i])) {
			if (jobs[i] >= max_jobs) {
				repeat = 1;
				break;
			}

			lp = list_entry(pm_list[i].next,
					struct lpurge_object_pack, lop_list);
			list_del(&lp->lop_list);

			/* now fork and execute */
			pid = lpurge_spawn_one(i, lp);
			if (pid > 0) {
				lp->lop_pid = pid;
				lp->lop_nr = i;
				list_add_tail(&lp->lop_list, &list);
			}

			jobs[i]++;
			total++;
		}
	}

wait:
	assert(total > 0);
	pid = waitpid(-1, &status, 0);
	if (pid < 0) {
		if (errno == EINTR)
			goto wait;
		fprintf(stderr, "error on waiting: %d\n", pid);
		exit(1);
	}
	lp = NULL;
	list_for_each_entry(tmp, &list, lop_list) {
		if (pid == tmp->lop_pid) {
			lp = tmp;
			break;
		}
	}
	if (!lp) {
		fprintf(stderr, "can't find child %d\n", pid);
		exit(1);
	}
	list_del(&lp->lop_list);
	i = lp->lop_nr;
	DEBUG("process %d completed for MDT #%d: %d\n",
		 pid, i, WEXITSTATUS(status));
	free(lp);
	assert(jobs[i] > 0);
	jobs[i]--;
	total--;
	if (repeat)
		goto next;
	if (total)
		goto wait;

	return 0;
}

void lpurge_purge_slot(struct lpurge_slot *ls, long long target)
{
	struct lpurge_object_pack *lp, *pm;
	struct list_head pm_list[MAX_MDTS];
	__u64 blocks[MAX_MDTS] = { 0 };
	__u64 total, was, kbfree;
	int i, rc;

	/* try to remove some replicas */

	for (i = 0; i < MAX_MDTS; i++)
		INIT_LIST_HEAD(&pm_list[i]);

again:
	DEBUG("want to release upto %llu (expect %lu in %lu)\n",
		target, ls->ls_space, ls->ls_found);
	total = 0;
	/* take few FIDs */
	assert(!list_empty(&ls->ls_obj_list));
	lp = list_entry(ls->ls_obj_list.next, struct lpurge_object_pack, lop_list);
	for (i = lp->lop_nr - 1; i >= 0; i--) {
		struct lu_fid fid;
		int idx;

		lp->lop_nr--;

		/* split by MDT index, we could do this at scanning, but that can
		 * result in many useless lookups if only part of slots are subject
		 * to removal */
		fid = lp->lop_fids[i];
		fid.f_ver = 0; /* lp_fids.f_ver stores blocks */

		/* XXX: local cache to avoid ioctl() for each FID? */
		rc = llapi_get_mdt_index_by_fid(lustre_fd, &fid, &idx);
		if (rc < 0) {
			DEBUG("can't lookup "DFID": rc=%d\n", PFID(&fid), rc);
			continue;
		}
		/* how many blocks we expect to free */
		blocks[idx] += lp->lop_fids[i].f_ver;
		total += lp->lop_fids[i].f_ver;
		ls->ls_space -= lp->lop_fids[i].f_ver;
		ls->ls_found--;
		ls->ls_stored--;

		/* put FID into per-MDT pack */
		pm = NULL;
		if (!list_empty(&pm_list[idx]))
			pm = list_entry(pm_list[idx].next,
					struct lpurge_object_pack, lop_list);
		if (!pm || pm->lop_nr >= pm->lop_max) {
#define FIDS_PER_CALL	128 /* XXX: bump to a bigger value */
#define PM_SIZE	(offsetof(struct lpurge_object_pack, lop_fids[FIDS_PER_CALL]))
			pm = malloc(PM_SIZE);
			assert(pm);
			pm->lop_max = FIDS_PER_CALL;
			pm->lop_nr = 0;
			list_add(&pm->lop_list, &pm_list[idx]);
		}
		pm->lop_fids[pm->lop_nr] = fid;
		pm->lop_nr++;

		/* if current collection of objects may free target space, then stop */
		if (total >= target)
			break;
	}
	if (!lp->lop_nr) {
		list_del(&lp->lop_list);
		free(lp);
	}

	/* fork, start and wait for completion against each MDT */
	/* give some time to OST_DESTROY to pass through */
	/* estimate how much space has been released */

	was = lpurge_kbfree();
	DEBUG("spawn, expect %llu blocks back\n", total);
	lpurge_spawn(pm_list);
	/* XXX: 20 is probably too short for ZFS as changes need to
	 * get committed on MDS first, then sent to OST, then get
	 * committed on OST. we could watch OST_DESTROY stats on MDS
	 * someway, but not sure this is really good approch */
	for (i = 0; i < 20; i++) {
		sleep(1);
		kbfree = lpurge_kbfree();
		if (kbfree > was && kbfree - was >= total)
			break;
	}
	DEBUG("got %llu back (now %llu, was %llu)\n",
	      kbfree - was, kbfree, was);
	if (kbfree > was)
		target -= kbfree - was;

	kbfree = lpurge_kbfree();
	if (target <= 0 || kbfree >= freehi) {
		/* got enough space back, relax */
		DEBUG("relax\n");
		return;
	}

	if (!list_empty(&ls->ls_obj_list)) {
		/* we still have some objects, try to get rid of them */
		goto again;
	}

	if (ls->ls_found == 0) {
		/* we processed all objects of this age, nothing left */
		return;
	}

	DEBUG("scan for more objects\n");

	/* if not enough space is released, but slot's counters suppose
	 * more space, then repeat scanning for this age only */
	for (i = 0; i < LPURGE_HIST_MAX; i++) {
		if (&lpurge_hist[i] == ls) {
			lpurge_hist[i].ls_found = 0;
			lpurge_hist[i].ls_space = 0;
			lpurge_hist[i].ls_stored = 0;
			lpurge_hist[i].ls_scan = 1;
		} else {
			lpurge_hist[i].ls_scan = 0;
		}
	}
	oldest = 0;
	lipe_scan(&instance, 1);
	if (!list_empty(&ls->ls_obj_list)) {
		/* we still have some objects, try to get rid of them */
		assert(ls->ls_found > 0);
		goto again;
	}

	/* XXX: VERY IMPORTANT CASE
	 * what if the scanning found N objects, but only M (<N) were stored
	 * because of the limit. we tried to release those M and failed for
	 * a reason (e.g. no replica yet). so we scan again and found/store
	 * the same M objects. livelock? should we remember the point where
	 * we stop to store objects so that subsequent scan can resume from
	 * that point? */

	return;
}

void lpurge_scan_and_free(void)
{
	unsigned long long kbfree;
	scan_start = time(NULL) * 1000;
	int i;

	DEBUG("now look for space\n");

	/* in V0 we scan the whole device, then process the results */
	/* we want all ages */
	for (i = 0; i < LPURGE_HIST_MAX; i++) {
		if (!list_empty(&lpurge_hist[i].ls_obj_list)) {
			DEBUG("slot %d is not empty: %lu in %lu\n",
				i, lpurge_hist[i].ls_space,
				lpurge_hist[i].ls_found);
			goto skip_scan;
		}
		lpurge_hist[i].ls_found = 0;
		lpurge_hist[i].ls_stored = 0;
		lpurge_hist[i].ls_space = 0;
		lpurge_hist[i].ls_scan = 1;
	}

	if (oldest != 0) {
		/* rebuild slots with finer grained ages
		 * as we know the oldest age now */
		unsigned long age = oldest;
		for (i = 0; i < LPURGE_HIST_MAX; i++) {
			age = age >> 1;
			if (age < 4)
				break;
		}
		for (i = 0; i < LPURGE_HIST_MAX; i++) {
			lpurge_hist[i].ls_age = age;
			age = age << 1;
		}
	}

	oldest = 0;
	/* we'll be measuring "age" since the scanning started
	 * so that the final histogram is uniform in this regard.
	 * notice that additional rescan from within lpurge_slot()
	 * for additional objects doesn't reset scan_started()
	 * to keep the histogram consistent with the initial scan */
	scan_started = time(NULL) * 1000;
	lipe_scan(&instance, 1);
	DEBUG("SCANNED: oldest %lu\n", oldest);

	for (i = 0; i < LPURGE_HIST_MAX; i++) {
		if (lpurge_hist[i].ls_found == 0)
			continue;
		DEBUG("%d (< %lu): %lu in %lu objects\n",
			i, lpurge_hist[i].ls_age,
			lpurge_hist[i].ls_space,
			lpurge_hist[i].ls_found);
	}

skip_scan:
	/* start from the oldest group */
	for (i = LPURGE_HIST_MAX - 1; i >= 0; i--) {
		struct lpurge_slot *ls = lpurge_hist + i;

		if (ls->ls_found == 0/* || ls->ls_space == 0*/)
			continue;

		kbfree = lpurge_kbfree();
		if (kbfree >= freehi) {
			/* got enough space back */
			break;
		}

		DEBUG("try to release slot %d\n", i);
		lpurge_purge_slot(ls, freehi - kbfree);
	}
}

void parse_mountpoint(const char *name)
{
	struct lu_fid fid;
	int rc;

	lustre_fd = open(name, O_RDONLY | O_DIRECTORY);
	if (lustre_fd < 0) {
		fprintf(stderr, "can't open %s: %d\n", name, errno);
		exit(1);
	}
	rc = llapi_fd2fid(lustre_fd, &fid);
	if (rc < 0) {
		fprintf(stderr, "%s isn't Lustre mountpoint: %d\n", name, rc);
		exit(1);
	}
}

void parse_mds(char *args)
{
	char *host, *mnt, *sidx;
	int idx;

	/* mds# hostname mountpoint */

	sidx = strsep(&args, ":\n");
	idx = atoi(sidx);
	if (idx >= MAX_MDTS) {
		fprintf(stderr, "too high MDS index\n");
		exit(1);
	}
	if (mds[idx].host) {
		fprintf(stderr, "mds #%d is defined already\n", idx);
		exit(1);
	}
	host = strsep(&args, ":\n");
	mnt = strsep(&args, ":\n");
	if (!host || !mnt) {
		fprintf(stderr, "no host or mntpt: %s\n", args);
		exit(1);
	}
	mds[idx].host = strdup(host);
	mds[idx].mnt = strdup(mnt);
	mdsnr++;
	DEBUG("add mds #%d at %s@%s\n", idx, host, mnt);
}

void load_config(char *name)
{
	char buf[PATH_MAX];
	FILE *f;

	f = fopen(name, "r");
	if (!f) {
		fprintf(stderr, "can't open config file %s\n", name);
		exit(1);
	}
	while (!feof(f)) {
		char *s, *t, *a;
		if (!fgets(buf, sizeof(buf), f))
			break;
		s = buf;
		while (*s == ' ' && *s != 0)
			s++;
		if (*s == '#')
			continue;
		t = strsep(&s, "=\n");
		if (!t || *t == 0)
			continue;
		a = strsep(&s, "\n");
		if (!a) {
			fprintf(stderr, "no argument for %s\n", t);
			exit(1);
		}

		if (!strcmp(t, "device")) {
			lpurge_find_device(a);
		} else if (!strcmp(t, "mount")) {
			parse_mountpoint(a);
		} else if (!strcmp(t, "freelo")) {
			freelo = atoi(a);
		} else if (!strcmp(t, "freehi")) {
			freehi = atoi(a);
		} else if (!strcmp(t, "max_jobs")) {
			max_jobs = atoi(a);
			/* XXX: where 1024 comes from? */
			if (max_jobs < 1 || max_jobs > 1024) {
				fprintf(stderr, "invalid max_jobs\n");
				exit(1);
			}
		} else if (!strcmp(t, "interval")) {
			check_interval = atoi(a);
		} else if (!strcmp(t, "debug")) {
			dfile = fopen(a, "a+");
		} else if (!strcmp(t, "mds")) {
			parse_mds(a);
		} else if (!strcmp(t, "pool")) {
			src_pool = strdup(a);
		} else {
			fprintf(stderr, "unknown tunable: %s\n", t);
			exit(1);
		}

	}
	fclose(f);
}


int main(int argc, char **argv)
{
	unsigned long long kbtotal;
	char buf[1024];
	int i;

	memset(mds, 0, sizeof(mds));

	if (argc < 2)
		usage(argv[0]);
	load_config(argv[1]);

	lpurge_read_param("fstype", buf, sizeof(buf));
	if (!strcmp(buf, "ldiskfs"))
		instance.li_fstype = LBT_LDISKFS;
	else if (!strcmp(buf, "zfs")) {
		instance.li_fstype = LBT_ZFS;
		fprintf(stderr, "no support for ZFS yet\n");
		exit(1);
	} else {
		fprintf(stderr, "Unknown fstype %s\n", buf);
		exit(1);
	}

	if (mdsnr == 0) {
		fprintf(stderr, "no MDS configured\n");
		exit(1);
	}
	if (!src_pool) {
		fprintf(stderr, "no pool specified\n");
		exit(1);
	}
	for (i = 0; i < mdsnr; i++) {
		if (!mds[i].host) {
			fprintf(stderr, "sparse MDS configuration\n");
			exit(1);
		}
	}

	lpurge_read_param("mntdev", buf, sizeof(buf));
	strcpy(instance.li_device, buf);
	instance.li_callback = lpurge_lipe_callback;

	kbtotal = lpurge_kbtotal();
	if (freelo < 1 || freelo > 99) {
		printf("Invalid free low threshold\n");
		exit(1);
	}
	freelo = freelo * kbtotal / 100;

	if (freehi < 1 || freehi > 99) {
		printf("Invalid free hi threshold\n");
		exit(1);
	}
	freehi = freehi * kbtotal / 100;
	if (freehi <= freelo) {
		printf("freehi < freelo\n");
		exit(1);
	}
	DEBUG("total: %llu, free lo: %llu hi %llu\n",
		kbtotal, freelo, freehi);

	lpurge_init_result();

	while (1) {
		/* XXX: do lazy scanning in the background
		 * 	before the real need, so that when
		 * 	low space is hit, we are ready
		 * 	to release some amount at least */

		/* what for low threshold */
		lpurge_wait_for_low();

		/* scan and purge */
		lpurge_scan_and_free();
		sleep(5);
	}

}

