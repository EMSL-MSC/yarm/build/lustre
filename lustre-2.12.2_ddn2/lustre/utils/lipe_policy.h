#include <assert.h>
#include <ext2fs/ext2fs.h>
#include <libcfs/util/list.h>
#include <lustre/include/uapi/linux/lustre/lustre_fid.h>


#define LDEBUG(fmt, args...)
#define LERROR printf
#define LINFO(fmt, args...)

#define LASSERT	assert

struct lipe_object_ldiskfs {
	ext2_filsys		 lol_fs;
	ext2_ino_t		 lol_ino;
	struct ext2_inode	*lol_inode;
};

#ifdef HAVE_ZFS
struct lipe_object_zfs {
	objset_t	*loz_objset;
	uint64_t	 loz_object;
};
#endif
enum lipe_backfs_type {
	LBT_LDISKFS = 0,
	LBT_ZFS,
	LBT_LAST
};

struct lipe_object {
	__u64					 lo_id;
	struct lipe_backfs_operations		*lo_backfs_ops;
	enum lipe_backfs_type			 lo_backfs_type;
	union {
		struct lipe_object_ldiskfs	 lo_ldiskfs;
#ifdef HAVE_ZFS
		struct lipe_object_zfs		 lo_zfs;
#endif
	} u;
};

struct scan_result {
	unsigned long long	sr_number_dirs;
	unsigned long long	sr_number_files;
	unsigned long long	sr_number_inodes;
	unsigned long long	sr_number_groups;
	struct timeval		sr_time_start;
	struct timeval		sr_time_end;
	struct timeval		sr_time_diff;
	unsigned long		sr_group_count;
	unsigned long		sr_group_end;
	unsigned long		sr_group_start;
};

struct global_info {
	/* common fields for both ldiskfs and ZFS */
	struct lu_fid		*gi_watch_fid;
	const char		*gi_workspace;
	bool			 gi_abort_failure;
	bool			 gi_all_inode;
	bool			 gi_lustre;
	union {
		struct {/* specific fields for ldiskfs */
			/* group that current thread scanning */
			unsigned long		 gi_group_current;
			/*
			 * the end of stopping scanning,
			 * equals to total groups
			 */
			unsigned long		 gi_group_end;
			/* group scan batch */
			unsigned long		 gi_group_batch;
			/* mutex used to protect group items */
			pthread_mutex_t		 gi_mutex;
		};
#ifdef HAVE_ZFS
		struct {/* specific fields for ZFS */
			struct lipe_policy	*gi_policy;
			const char		*gi_srvname;
			objset_t		*gi_objset;
			struct lipe_instance	*gi_instance;
		};
#endif
	};
};

#define MAX_COUNTER_NAME	(4096)

struct lipe_counter {
	/** Name */
	char				 lc_name[MAX_COUNTER_NAME];
	/** Count number */
	__u64				 lc_count;
	/** Flist to dump, could be empty */
	struct lipe_flist		*lc_flist;
	/** Point to type */
	struct lipe_counter_type	*lc_type;
	/**
	 * Linked to lct_counters
	 */
	struct list_head		 lc_linkage;
	/* Used for classify */
	int64_t				 lc_value;
};

struct classify_list {
	struct lipe_classify	*cl_classifys;
	int			 cl_classify_number;
};

struct counter_list {
	struct lipe_counter	*cl_counters;
	int			 cl_counter_number;
};

struct thread_info {
	struct global_info	*ti_global_info;
	/* ID returned by pthread_create() */
	pthread_t		 ti_thread_id;
	/* Application-defined thread index */
	int			 ti_thread_index;
	struct lipe_instance	*ti_instance;
	struct lustre_disk_data	*ti_ldd;
	struct lipe_policy	*ti_policy;
	struct scan_result	 ti_result;
	bool			 ti_started;
	bool			 ti_stopping;
	bool			 ti_stopped;
	struct counter_list	 ti_counter_list;
	struct classify_list	 ti_classify_list;
	void			*private;
};

struct lipe_policy_sysattrs {
	int64_t	lps_sys_time;
	__u64	lps_attr_bits;
};


enum {
	LIPE_POLICY_BIT_ATTR		= 1 << 0, /* Attr in ext2_ino_t */
	LIPE_POLICY_BIT_LINKEA		= 1 << 1, /* XATTR_NAME_LINK */
	LIPE_POLICY_BIT_LMAEA		= 1 << 2, /* XATTR_NAME_LMA */
	LIPE_POLICY_BIT_HSMEA		= 1 << 3, /* XATTR_NAME_HSM */
	LIPE_POLICY_BIT_LOVEA		= 1 << 4, /* XATTR_NAME_LOV */
	/*
	 * The file size on MDT is always 0 (without Data on MDT). But Lazy
	 * Size on MDT can be used as an estimated size since the inaccuracy
	 * doesn't bother the policy engine. So, if DoM, use real size; if
	 * LSoM exists, use it as file size.
	 */
	LIPE_POLICY_BIT_SIZE		= 1 << 5,
	LIPE_POLICY_BIT_SOM		= 1 << 6, /* XATTR_NAME_SOM */
	/*
	 * The DoM status. This dosn't require reading any other xattr except
	 * XATTR_NAME_LOV, but needs to futher parse lum.
	 */
	LIPE_POLICY_BIT_DOM		= 1 << 7,
	LIPE_POLICY_BIT_FID		= 1 << 8,
};

enum {
	LIPE_POLICY_BIT_SYSATTR_TIME	= 1 << 0, /* System time */
};

enum lustre_dom_status {
	/* LiPE is built on Lustre without PFL support, so don't know exactly */
	LDS_UNKNOWN = 0,
	/* no Data on MDT at all */
	LDS_NO_DOM,
	/* only Data on MDT, no data on OST */
	LDS_DOM_ONLY,
	/* Data both on MDT and OST */
	LDS_DOM_OST
};

#define MAX_LINKEA_SIZE 65536
struct lipe_policy_attrs {
        int64_t                  lpa_atime_ms;
        int64_t                  lpa_mtime_ms;
        int64_t                  lpa_ctime_ms;
        int64_t                  lpa_size;
        int64_t                  lpa_mode;
        int64_t                  lpa_uid;
        int64_t                  lpa_gid;
        int64_t                  lpa_blocks;
        int64_t                  lpa_flags;
        int64_t                  lpa_nlinks;
        struct lu_fid            lpa_fid;
        __u64                    lpa_attr_bits;
        /* Buffer for struct link_ea_header */
        char                     lpa_leh_buf[MAX_LINKEA_SIZE];
        struct lov_user_md      *lpa_lum;
        int                      lpa_lum_size;
        struct hsm_user_state    lpa_hsm_state;
	enum lustre_dom_status	 lpa_dom_status;
#ifdef HAVE_LAZY_SIZE_ON_MDT
        struct lustre_som_attrs  lpa_som;
#endif
	struct filter_fid	 lpa_ff;
	int			 lpa_ff_size;
};

struct lipe_backfs_operations {
	int (*get_lma_ea)(struct lipe_object *object,
			  struct lipe_policy_attrs *attrs);
	int (*get_link_ea)(struct lipe_object *object,
			   struct lipe_policy_attrs *attrs);
	int (*get_hsm_ea)(struct lipe_object *object,
			  struct lipe_policy_attrs *attrs,
			  bool watch);
	int (*get_lum_ea)(struct lipe_object *object,
			  struct lipe_policy_attrs *attrs,
			  bool watch);
	int (*get_som_ea)(struct lipe_object *object,
			  struct lipe_policy_attrs *attrs,
			  bool watch);
	int (*get_fid_ea)(struct lipe_object *object,
			  struct lipe_policy_attrs *attrs);
};


struct lipe_policy {
	/** The groups in this policy */
	struct list_head	lp_groups;
	/** List of lct_linkage */
	struct list_head	lp_counters;
	/** Number of counters */
	int			lp_counter_number;
	/**
	 * The classify types in this policy, used for when initing
	 * classify
	 */
	struct list_head	lp_classifys;
	/** Number of classifys */
	int			lp_classify_number;
	/** Whether dump more file lists */
	bool			lp_dump_flist;
};

struct lipe_instance {
	/** Device name */
	char                      li_device[PATH_MAX];
	enum lipe_backfs_type	  li_fstype;
	/** The group number for this instance */
	int                       li_group_number;
	/** The groups for this instance */
	struct lipe_rule_group	**li_groups;
	int (*li_callback)(struct lipe_instance *instance, struct lipe_object *object,
			   struct lipe_policy_attrs *attrs);
};

static inline void counter_list_sum(struct counter_list *sum,
		      struct counter_list *list)
{
	int i;

	//LASSERT(list->cl_counter_number == sum->cl_counter_number);
	for (i = 0; i < list->cl_counter_number; i++)
		sum->cl_counters[i].lc_count += list->cl_counters[i].lc_count;
}

void *ldiskfs_scan_thread(void *arg);
int lipe_policy_apply(struct lipe_object *object,
		      struct lipe_policy *policy,
		      struct lipe_instance *instance,
		      struct lipe_policy_attrs *attrs,
		      struct lipe_policy_sysattrs *sysattrs,
		      struct scan_result *scan_result,
		      const char *srv_name,
		      bool abort_failure,
		      struct counter_list *counter_list,
		      struct classify_list *classify_list,
		      struct lu_fid *watch_fid,
		      bool all_object,
		      bool lustre);
void lipe_policy_attrs_reset(struct lipe_policy_attrs *attrs);
int lipe_policy_attrs_init(struct lipe_policy_attrs *attrs);
void lipe_policy_attrs_fini(struct lipe_policy_attrs *attrs);
int decode_linkea(__u64 object, char *buf, ssize_t size);
int lustre_hsm2user(struct hsm_attrs *disk, struct hsm_user_state *hus);
int decode_lum(struct lov_user_md *lum, ssize_t size);

int scan_threads_start(struct thread_info **pinfo, int num_threads,
		       struct lipe_instance *instance,
		       struct lipe_policy *policy,
		       struct global_info *global_info,
		       enum lipe_backfs_type backfs_type);
int scan_threads_join(struct thread_info *infos, int num_threads,
		      struct scan_result *result,
		      struct counter_list *sum_counter_list,
		      struct classify_list *sum_classify_list,
		      enum lipe_backfs_type backfs_type);

static inline void diff_timevals(struct timeval *start,
				 struct timeval *end,
				 struct timeval *out)
{
	out->tv_sec = end->tv_sec - start->tv_sec;
	out->tv_usec = end->tv_usec - start->tv_usec;
	if (start->tv_usec > end->tv_usec) {
		out->tv_sec--;
		out->tv_usec += 1000000;
	}
}

int ldiskfs_scan(struct lipe_instance *instance, struct lipe_policy *policy,
		 struct scan_result *result,
		 struct counter_list *sum_counter_list,
		 struct classify_list *sum_classify_list,
		 int num_threads, const char *workspace,
		 bool abort_failure);

static int inline lipe_scan(struct lipe_instance *instance, int num_threads)
{
	struct classify_list sum_classify_list;
	struct counter_list sum_counter_list;
	struct lipe_policy policy;
	struct scan_result result;
	int rc = 0;

	memset(&result, 0, sizeof(result));
	if (instance->li_fstype == LBT_LDISKFS)
		rc = ldiskfs_scan(instance, &policy, &result, &sum_counter_list,
			&sum_classify_list, num_threads, "test", true);
	else if (instance->li_fstype == LBT_ZFS) {
		assert(0);
	}

	return rc;

}

