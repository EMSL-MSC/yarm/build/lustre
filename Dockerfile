FROM centos:7

ARG kernelver
ARG ver

RUN yum clean all && yum -y update 

RUN yum -y install kernel-${kernelver} kernel-devel-${kernelver} kernel-headers-${kernelver} kernel-tools-${kernelver}.x86_64 redhat-lsb-core redhat-rpm-config gcc autoconf automake make libtool libtool-ltdl-devel python-docutils rpm-build rpmdevtools zlib-devel libuuid-devel libblkid-devel libselinux-devel parted lsscsi wget dkms gcc perl make libtool e2fsprogs-devel wget kernel-debug-debuginfo git libyaml-devel openssl-devel

COPY lustre-${ver}.tar.gz /root/

RUN rpmdev-setuptree && rm /root/.rpmmacros
COPY lustre-${ver}/rpm/* /root/rpmbuild/SOURCES/
COPY lustre-${ver}/rpm/* /root/
#really bad hack until lustre can deal with it
COPY pci-dma.h /usr/src/kernels/3.10.0-1062.1.2.el7.x86_64/include/linux/

RUN rpmbuild -vv -ta /root/lustre-${ver}.tar.gz --define "kver ${kernelver}.x86_64" --define "kernel ${kernelver}.x86_64" --without servers --with lustre_utils --without initramfs --with lustre_modules --without lustre_tests --with lnet_dlc --without ofed --without mlnx

CMD ls -l /root/rpmbuild/RPMS/x86_64/
